import React, { Component } from "react";
import { View, Text, StyleSheet, ScrollView, FlatList } from "react-native";
import { Actions } from "react-native-router-flux";
import HTML from 'react-native-render-html';

class PracticeScoreSkipped extends Component {
    practiceReview() {
        Actions.practiceReview();
      }

    getScoreSkipped = (obj) => {
        console.log("nak tau obj dalam scoreskipped")
        console.log(obj)
        return (
          <View style={styles.container}>
            <View style={{width:'100%', height:'100%', alignContent:'center', alignItems:'center'}}>
                <FlatList 
                    data={obj}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item }) => (
                    <View style={styles.cardContainer}>
                    <View style={{padding: 20,}}>
                        <View style={{flexDirection:'row', width:'100%'}}>
                            <View style={{width:'5%'}}>
                                <Text>
                                    {item.question_no}
                                </Text>
                            </View>
                            <View style={{width:'95%'}}>
                                <HTML html={item.question_text} />
                            </View>
                        </View>
    
                        <View style={{flexDirection:'row',width:'100%', marginTop:10}}>
                            <View style={{width:'10%'}}>
                                <Text style={{color:'green'}}>
                                    Ans:
                                </Text>
                            </View>
                            <View style={{width:'90%'}}>
                                    { item.answer[0].answer_status == true ? <HTML html={item.answer[0].answer_text} /> : null}
                                    { item.answer[1].answer_status == true ? <HTML html={item.answer[1].answer_text} /> : null}
                                    { item.answer[2].answer_status == true ? <HTML html={item.answer[2].answer_text} /> : null}
                                    { item.answer[3].answer_status == true ? <HTML html={item.answer[3].answer_text} /> : null}
                            </View>
                        </View>
    
                        <View style={{flexDirection:'row',width:'100%', marginTop:10}}>
                            <View style={{width:'25%'}}>
                                <View>
                                <View style={{borderWidth:2, borderColor:'cyan', borderRadius:15, alignItems:'center'}}> 
                                    <Text style={{padding:10}}>
                                        {item.difficulty_level}
                                    </Text>
                                </View>
                                </View>
                            </View>
                            <View style={{width:'75%', alignItems:'flex-end'}}>
                                <Text style={{color:"purple", paddingTop:10}} onPress={() => this.practiceReview(obj)}>
                                    REVIEW
                                </Text>
                            </View>
                        </View>
                    </View>
                </View> 
                    )}
                />
                </View>
          </View>
        );
        
    }
    render() { 
        return null;
    }
} 

const styles = StyleSheet.create({
    container:{
        width:'100%',
        height:'100%',
        alignItems:'center',
        marginTop:10,
        marginBottom:10
        
    },
    cardContainer:{
        width:'98%',
        backgroundColor:"white",
        elevation:10,
        alignItems:'center',
        borderBottomColor: 'silver',
        borderBottomWidth: 1,
    }
})

 
export default PracticeScoreSkipped;


import React,{Component} from 'react';
import {View, Text, StyleSheet, FlatList, ScrollView, Image} from 'react-native';
import * as Progress from "react-native-progress";
import { getStore } from "../live_api/Utilities";

class SubjectiveReport extends Component{

    constructor(props) {
        super(props);
        this.state = {
          isLoading: true,
          isSelected: false
        };
      }
    
      componentDidMount() {
        getStore("subject_list").then(value => {
          this.setState({
            isLoading: false,
            dataSource: JSON.parse(value)
          });
        });
      }

    render(){
        return (
            <View style={styles.container}>
                  <FlatList
                    data={this.state.dataSource}
                    renderItem={({ item }) => (
                        <View style={{alignItems:'center', }}>
                        <View style={styles.practiceCard}>
                          <View style={{flexDirection:'row', width:'100%'}}>
                          <View style={{width:'15%', backgroundColor:'green'}}>
                              {
                                  item.name == 'Bahasa Melayu' ? <Image source={require("../images/subject_bahasa_melayu.jpg")} style={{width:110,height:100}}/> : 
                                  item.name == 'Bahasa Inggeris' ? <Image source={require("../images/subject_english.jpg")} style={{width:110,height:100}}/> :
                                  item.name == 'Matematik' ? <Image source={require("../images/subject_math.jpg")} style={{width:110,height:100}}/> :
                                  item.name == 'Sains' ? <Image source={require("../images/subject_science.jpg")} style={{width:110,height:100}}/> :
                                  null
                              }
                          </View>
                          <View style={{padding:15, width:'85%'}}>
                          <Text style={{}}>{item.name}</Text>
                          {console.log(item.name)}
                          <Text style={{paddingTop:10}}>540 Session</Text>
                          <View style={{flexDirection:'row', width:'100%', paddingTop:10}}>
                              <View style={{width:'80%'}}>
                              <Progress.Bar
                                progress={0.2}
                                width={null}
                                color={"#6cc071"}
                                unfilledColor={"#ddd"}
                                borderColor={"transparent"}
                              />
                              </View>
                              <View style={{width:'20%', alignItems:'center'}}>
                                  <Text style={{textAlign:'center'}}>25%</Text>
                              </View>
                          </View>
                          </View>
                          </View>
                        </View>
                        </View>
                    )}
                    keyExtractor={ (item, index) => index.toString() }
                  />
            </View>
          );
    }
}

const styles = StyleSheet.create({
    container: {
        width:'100%',
        height:'100%'
    },
    practiceCard: {
      width: "100%",
      width:'95%',
      backgroundColor: "white",
      elevation: 10,
      marginTop: 10,
    }
  });

export default SubjectiveReport;
import React, {Component} from 'react';
import {AppRegistry, StyleSheet,Text,View, TouchableHighlight, Button} from 'react-native';
import { Stopwatch, Timer } from 'react-native-stopwatch-timer'
import Dialog, { SlideAnimation,DialogContent } from 'react-native-popup-dialog';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
 

class CountUpTimer extends Component{
  constructor(props) {
    super(props);
    this.state = {
      timerStart: false,
      stopwatchStart: true,
      totalDuration: 90000,
      timerReset: false,
      stopwatchReset: false,
    };
    this.toggleTimer = this.toggleTimer.bind(this);
    this.resetTimer = this.resetTimer.bind(this);
    this.toggleStopwatch = this.toggleStopwatch.bind(this);
    this.resetStopwatch = this.resetStopwatch.bind(this);
  }

  toggleTimer() {
    this.setState({timerStart: !this.state.timerStart, timerReset: false});
  }

  resetTimer() {
    this.setState({timerStart: false, timerReset: true});
  }

  toggleStopwatch() {
    this.setState({stopwatchStart: !this.state.stopwatchStart, stopwatchReset: false});
  }

  resetStopwatch() {
    this.setState({stopwatchStart: false, stopwatchReset: true});
  }
  
  getFormattedTime(time) {
      this.currentTime = time;
  };
      render() {
        return (
          <View>

          <Stopwatch laps start={this.state.stopwatchStart}
            reset={this.state.stopwatchReset}
            options={options}
            getTime={this.getFormattedTime} />
        </View>
        );
      }
    }
    
    const handleTimerComplete = () => alert("custom completion function");

    const options = {
      container: {
        padding: 5,
        borderRadius: 5,
       
      },
      text: {
        color: '#FFF',
        marginLeft: 7,
      }
    };

export default CountUpTimer;

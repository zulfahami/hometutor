import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableWithoutFeedback,
  ScrollView,
  TouchableOpacity,
  Alert,
  Platform,
  FlatList
} from "react-native";
import { Actions } from "react-native-router-flux";
import PropTypes from "prop-types";
import { images } from "../components";
import { getStore, setStore } from "../live_api/Utilities";

const propTypes = {
  item: PropTypes.object
};

class PracticeItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      level_id: "",
      subject_list: "",
      user_id: "",
      isSelected: false
    };
  }

  componentDidMount = () => {
    // console.log('in did mount');
    // getStore("subject_list").then(value => {
    //   this.setState({ subject_list: JSON.parse(value) }); })
    // .then(value => {
    //   getStore("level_id")
    //   .then(value => {
    //     this.setState({ level_id: JSON.parse(value) });
    //     console.log("levelsss_"+JSON.parse(value));
    //   })
    //   .then(value => {
    //     console.log("subject_list : "+this.state.subject_list);
    //     console.log("subject_size : "+this.state.subject_list.length);
    //     let i;
    //     // for(i=0;i<this.state.subject_list.length;i++){
    //       // let subjectID = this.state.subject_list[i].id;
    //       // let levelID = this.state.level_id;
    //       // let url = `http://api.hometutor.com.my/mobile_api/index.php?process=topic&subject_id=${subjectID}&level_id=${levelID}&question_type=1`;
    //       let url = `http://api.hometutor.com.my/mobile_api/index.php?process=topic&subject_id=5&level_id=10&question_type=1`;
    //       doRecursiveRequestFunction(url, 1);
    //       // .then(data => {
    //       //   console.log("resultsss : "+data);
    //       //   // this.setState({...this.state, topicList : data});
    //       //   // return data;
    //       // } )
    //       // .catch(error => console.log(error));
    //     // }
    //     // for(i=0;i<this.state.subject_list.length;i++){
    //     //   // console.log("value_i : "+i);
    //     //   // console.log(this.state.subject_list[i].id+"-"+this.state.subject_list[i].name+"-"+this.state.subject_list[i].abbr);
    //     //   let subjectID = this.state.subject_list[i].id;
    //     //   let levelID = this.state.level_id;
    //     //   // console.log(`http://api.hometutor.com.my/mobile_api/index.php?process=topic&subject_id=${subjectID}&level_id=${levelID}&question_type=1`);
    //     //   fetch(
    //     //     `http://api.hometutor.com.my/mobile_api/index.php?process=topic&subject_id=${subjectID}&level_id=${levelID}&question_type=1`,
    //     //     {
    //     //       method: "POST",
    //     //       headers: {
    //     //       Accept: "application/json",
    //     //       "Content-Type": "application/json"
    //     //       },
    //     //       body: JSON.stringify({})
    //     //     }
    //     //   )
    //     //   .then(response => response.json())
    //     //   .then(responseJson => {
    //     //     var itemData = responseJson.items;
    //     //     this.setState({...this.state, topicList : itemData})
    //     //     // this.setState({arrayvar:[...this.state.arrayvar, newelement]});
    //     //     if(itemData != null){
    //     //       setStore("topic_"+levelID+"_"+subjectID, itemData.topic);
    //     //     }
    //     //     // console.log("topic_"+levelID+"_"+subjectID+" : "+JSON.stringify(itemData.topic));
    //     //   })
    //     //   .catch(error => {
    //     //     console.error(error);
    //     //   });
    //     // }
    //     console.log('Test topic list')
    //     console.log(this.state.topicList)
    //   });
    // })
    console.log('in did mount PRACTICE ITEM')
    console.log(this.props.item)
  };

  questionInstruction() {
    Actions.questionInstruction();
  }
  state = {};

  onPressHandler = id => {
    console.log(id);
    this.setState((prevState, prevProps) => ({
      isSelected: !prevState.isSelected
    }));
  };

  renderDetails = () => (
    <FlatList
      data={this.props.item}
      renderItem={({ item }) => (
        <View style={{ width: "90%", elevation: 2, backgroundColor: "white" }}>
          <View
            style={{ flexDirection: "row", paddingBottom: 10, width: "100%" }}
          >
            <View style={{ width: "15%" }}>
              <Image
                source={require("../images/icon_file_in_box.png")}
                style={{ height: 45, width: 45 }}
              />
            </View>
            <View style={{ paddingLeft: 20, width: "65%" }}>
              <Text style={styles.description}>{item.name}</Text>
              <Text>10 total question answered</Text>
            </View>
            <View style={{ alignItems: "center", width: "20%" }}>
              <TouchableOpacity onPress={this.questionInstruction}>
                <View
                  style={{
                    paddingTop: 6,
                    paddingBottom: 6,
                    borderWidth: 1,
                    borderRadius: 3,
                    borderColor: "green",
                    paddingLeft: 10,
                    paddingRight: 10
                  }}
                >
                  <Text style={{ color: "green" }}>Start</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      )}
      keyExtractor={(item, index) => index.toString()}
    />

    // <ScrollView>
    //   {this.Array_Items.map((item, key) => (
    //     <View key={key} style={styles.item}>
    //       <Text
    //         style={styles.item_text_style}
    //         onPress={() => {
    //           Alert.alert(item.toString());
    //         }}
    //       >
    //         {item}
    //       </Text>

    //       <View style={styles.item_separator} />
    //     </View>
    //   ))}
    // </ScrollView>
  );

  render() {
    let { isSelected } = this.state;
    return (
      <View
        style={{
          width: "100%",
          elevation: 10,
          marginTop: 10,
          borderRadius: 2,
          marginBottom: 10
        }}
      >
        <View style={{ alignItems: "center" }}>
          <View
            style={{
              width: "95%",
              backgroundColor: "white",
              alignItems: "center"
            }}
          >
            <View
              style={{
                width: "90%",
                alignItems: "center",
                paddingTop: 20,
                paddingBottom: 10,
                borderBottomWidth: 0.5,
                borderColor: "green"
              }}
            >
              <Text>{this.props.item.name}</Text>
            </View>
            <TouchableOpacity
              onPress={() => this.onPressHandler(this.props.item.id)}
            >
              <View
                style={{
                  flexDirection: "row",
                  width: "90%",
                  paddingTop: 10,
                  paddingBottom: 10
                }}
              >
                <View style={{ width: "50%", backgroundColor: "white" }}>
                  <Text style={styles.title}>Topic</Text>
                  {console.log(this.props.item.id)}
                </View>

                <View
                  style={{
                    width: "50%",
                    backgroundColor: "white",
                    alignItems: "flex-end"
                  }}
                >
                  <Image
                    source={
                      this.state.isSelected === false
                        ? images.expand
                        : images.close
                    }
                    style={styles.image}
                  />
                </View>
              </View>
            </TouchableOpacity>
            <View style={{ width: "100%", alignItems: "center" }} data={this.props.item}>
              {isSelected && this.renderDetails(this.props.item)}
            </View>
            <View />
          </View>
        </View>
      </View>
    );
  }
}

PracticeItem.propTypes = propTypes;

const styles = StyleSheet.create({
  titleContainer: {
    flexDirection: "row",
    justifyContent: "flex-end"
  },
  image: {
    height: 20,
    width: 20
  },
  title: {
    flex: 1
  },
  description: {},

  MainContainer: {
    flex: 1,
    paddingTop: Platform.OS === "ios" ? 20 : 0
  },

  item_text_style: {
    fontSize: 20,
    color: "#000",
    padding: 10
  },

  item_separator: {
    height: 1,
    width: "100%",
    backgroundColor: "#263238"
  }
});

export { PracticeItem };

import React, { Component } from "react";
import {
  View,
  Text,
  SafeAreaView,
  FlatList,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
  AsyncStorage
} from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import Entypo from "react-native-vector-icons/Entypo";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import { Actions } from "react-native-router-flux";

const getStore = async key => {
	var findData = "";
	try {
	  findData = (await AsyncStorage.getItem(key)) || "none";
	  return findData;
	} catch (error) {
	  // Error retrieving data
	  console.log(error.message);
	}
  };

class MenuDrawer extends Component {
  constructor(props) {
    super(props);
    this.state = { sFullname: "", sPackageName: "" };
  }

  componentDidMount() {
    getStore("profile_fullname").then(value => { this.setState({ sFullname: JSON.parse(value) });
    }).then(value => { getStore("package_name").then(value => { this.setState({ sPackageName: JSON.parse(value) }); })
    });
  }

  dashboard(){
    Actions.dashboard();
  }
  practice() {
    Actions.practice();
  }

  subjective() {
    Actions.subjective();
  }

  report(){
    Actions.report();
  }

  studentProfile(){
    Actions.studentProfile();
  }

  parentProfile(){
    Actions.parentProfile();
  }

  subscription(){
    Actions.subscription();
  }

  setting(){
    Actions.setting();
  }

  download(){
    Actions.download();
  }

  logout(){
    Actions.login();
  }

  render() {
    return (
      <View>
        <ScrollView>
        <View style={styles.backgroundImage}>
          <Image
            source={require("../images/bg_blue.jpg")}
            style={{ width: 300, height: 200 }}
          />
          <View style={{ flexDirection: "row", position: "absolute" }}>
            <View style={{ paddingLeft: 20 }}>
              <View style={styles.imageProfileCircle}>
                <Image
                  source={require("../images/default_user.png")}
                  style={{ width: 90, height: 90 }}
                />
              </View>
            </View>
            <View style={{ justifyContent: "center", paddingLeft: 10 }}>
              <Text style={{ color: "white" }}>{this.state.sFullname}</Text>
              <Text style={{ color: "white" }}>{this.state.sPackageName}</Text>
            </View>
          </View>
        </View>
        <View style={{ borderBottomWidth: 0.5, borderColor: "silver" }}>
          <TouchableOpacity onPress={this.dashboard}>
          <View style={{ flexDirection: "row", padding: 15 }}>
            <View>
              <MaterialCommunityIcons name="poll-box" size={22} />
            </View>
            <View style={{ paddingLeft: 30 }}>
              <Text>Dashboard</Text>
            </View>
          </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={this.practice}>
          <View style={{ flexDirection: "row", padding: 15 }}>
            <View>
              <MaterialCommunityIcons name="pencil" size={22} />
            </View>
            <View style={{ paddingLeft: 30 }}>
              <Text>Practice</Text>
            </View>
          </View>       
          </TouchableOpacity>

          <TouchableOpacity onPress={this.subjective}>
          <View style={{ flexDirection: "row", padding: 15 }}>
            <View>
              <Entypo name="graduation-cap" size={22} />
            </View>
            <View style={{ paddingLeft: 30 }}>
              <Text>Subjective</Text>
            </View>
          </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={this.report}>
          <View style={{ flexDirection: "row", padding: 15 }}>
            <View>
              <MaterialCommunityIcons name="flag" size={22} />
            </View>
            <View style={{ paddingLeft: 30 }}>
              <Text>Report</Text>
            </View>
          </View>
          </TouchableOpacity>
        </View>

        <View style={{ padding: 12 }}>
          <Text>Others</Text>
        </View>

        <View>
          <TouchableOpacity onPress={this.studentProfile}>
          <View style={{ flexDirection: "row", padding: 15 }}>
            <View>
              <MaterialCommunityIcons name="account" size={22} />
            </View>
            <View style={{ paddingLeft: 30 }}>
              <Text>Profile</Text>
            </View>
          </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={this.parentProfile}>
          <View style={{ flexDirection: "row", padding: 15 }}>
            <View>
              <MaterialCommunityIcons name="account-multiple" size={22} />
            </View>
            <View style={{ paddingLeft: 30 }}>
              <Text>Parent Info</Text>
            </View>
          </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={this.subscription}>
          <View style={{ flexDirection: "row", padding: 15 }}>
            <View>
              <MaterialCommunityIcons name="cart" size={22} />
            </View>
            <View style={{ paddingLeft: 30 }}>
              <Text>Subscription</Text>
            </View>
          </View>
          </TouchableOpacity>

          {/* <TouchableOpacity onPress={this.setting}>
          <View style={{ flexDirection: "row", padding: 15 }}>
            <View>
              <MaterialCommunityIcons name="wrench" size={22} />
            </View>
            <View style={{ paddingLeft: 30 }}>
              <Text>Setting</Text>
            </View>
          </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={this.download}>
          <View style={{ flexDirection: "row", padding: 15 }}>
            <View>
              <MaterialCommunityIcons name="cloud-download" size={22} />
            </View>
            <View style={{ paddingLeft: 30 }}>
              <Text>Download</Text>
            </View>
          </View>
          </TouchableOpacity> */}

          <TouchableOpacity onPress={this.logout}>
          <View style={{ flexDirection: "row", padding: 15 }}>
            <View>
              <MaterialCommunityIcons name="logout" size={22} />
            </View>
            <View style={{ paddingLeft: 30 }}>
              <Text>Log out</Text>
            </View>
          </View>
          </TouchableOpacity>

        </View>
        </ScrollView>
      </View>
    );
  }
}

export default MenuDrawer;

const styles = StyleSheet.create({
  backgroundImage: {
    position: "relative",
    top: 0,
    left: 0,
    width: 300,
    justifyContent: "center"
  },

  imageProfileCircle: {
    height: 90,
    width: 90,
    overflow: "hidden",
    backgroundColor: "silver",
    borderRadius: 90 / 2,
    borderWidth: 1,
    borderColor:'purple'
  }
});

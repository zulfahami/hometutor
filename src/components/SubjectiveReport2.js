import React, { Component } from "react";
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  ScrollView,
  Alert,
  ActivityIndicator,
  FlatList,
  TouchableOpacity
} from "react-native";
import { PracticeItem } from "../components/PracticeItem";
import { getStore } from "../live_api/Utilities";
import { images } from "../components";
import PropTypes from "prop-types";
import { Actions } from "react-native-router-flux";
import * as Progress from "react-native-progress";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

const propTypes = {
  item: PropTypes.object
};

class SubjectiveReport2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      level_id: "",
      subject_list: "",
      user_id: "",
      isSelected: false,
      topicList: [],
      sessionList: []
    };
  }

  subjectiveDetails(item) {
    console.log(item);
    Actions.reportDetailsSubjective(item);
  }

  componentDidMount() {
    getStore("subject_list").then(value => {
      this.setState({
        isLoading: false,
        dataSource: JSON.parse(value)
      });
    });

      getStore("all_login_data").then(value => { 
        let dataUser = JSON.parse(value); 
        let userId = dataUser.profile.user_id;
        let packageId = dataUser.packages.id;
        console.log(`http://api.hometutor.com.my/mobile_api/index.php?process=scorecard_new_subjective_rn&package_id=${packageId}&user_id=${userId}`);
        fetch(
          `http://api.hometutor.com.my/mobile_api/index.php?process=scorecard_new_subjective_rn&package_id=${packageId}&user_id=${userId}`,
          {
            method: "POST",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json"
            },
            body: JSON.stringify({})
          }
        )
          .then(response => response.json())
          .then(responseJson => {
            var itemData = responseJson.items.scorecard;
            this.setState({ sessionList: itemData, user_id: userId });
          })
          .catch(error => {
            console.error(error);
          });
  
      }).catch(error => {
        console.error(error);
      });


  }

  onPressHandler = id => {
    // console.log(id);
    this.state.dataSource.map(item => {
      if (item.id == id) {
        item.isSelected == undefined
          ? (item.isSelected = true)
          : item.isSelected == true
          ? (item.isSelected = false)
          : item.isSelected == false
          ? (item.isSelected = true)
          : null;
      }
    });
    this.setState({ ...this.state });
  };

  //flatlist topic
  renderDetails = (sessionList, subjectId) => {
    let countTotal = sessionList.subjective.length;
    return (
      <View style={{ width: "95%" }}>
      
        <FlatList
          data={JSON.parse(JSON.stringify(sessionList.subjective))}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item,index }) => (
            <View
              style={{ flexDirection: "row", paddingBottom: 10, width: "100%",
              borderColor: "#dddddd",
              borderWidth: 1,
              backgroundColor:'#fff'
            }}
            >
              <View style={{width:'10%',padding:5}}>
                <MaterialCommunityIcons name="account-box" size={22} />
              </View>
              <View style={{width: "90%",padding:5 }}>
                <TouchableOpacity
                  onPress={() =>
                    this.subjectiveDetails({
                      item: {
                        session_id: item.session_id,
                        topic_id: item.id,
                        level_id: 10,
                        subject_id: subjectId,
                        user_id: this.state.user_id,
                        session_text: (countTotal - index)
                      }
                    })
                  }
                >
              <View>
                <Text style={{fontSize:20}}> Session {countTotal - index} </Text>
                <Text style={{fontSize:13,color:'#aaaaaa'}}> {item.date} </Text>
              </View>

              {/* <View style={{ alignItems: "center", width: "20%" }}>
                <TouchableOpacity
                  onPress={() =>
                    this.subjectiveDetails({
                      item: {
                        session_id: item.session_id,
                        topic_id: item.id,
                        level_id: 10,
                        subject_id: subjectId,
                        user_id: this.state.user_id,
                        session_text: (countTotal - index)
                      }
                    })
                  }
                > */}
                  {/* <View
                  >
                    <Text>{item.score}%</Text>
                  </View> */}
                </TouchableOpacity>
              </View>
            </View>
          )}
        />
      </View>
    );
  };

  //flagtlist subject
  render() {
    const { isSelected } = this.state;
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, paddingTop: 20 }}>
          <ActivityIndicator />
        </View>
      );
    }
    else{

    return (
      <SafeAreaView>
        <ScrollView>
            <View>
              <FlatList
                data={this.state.sessionList}
                extraData={this.state}
                renderItem={({ item, index }) => (
                  
                  <View
                    style={{
                      width: "100%",
                      elevation: 10,
                      marginTop: 10,
                      borderRadius: 2,
                      marginBottom: 10,
                      alignItems: "center"
                    }}
                  >
                  <TouchableOpacity onPress={() => this.onPressHandler(item.subject_id)} >
                <View style={styles.practiceCard}>
                  <View style={{flexDirection:'row', width:'100%'}}>
                    <View style={{width:'30%', backgroundColor:'green'}}>
                        {
                            item.subject_name == 'Bahasa Melayu' ? <Image source={require("../images/subject_bahasa_melayu.jpg")} style={{width:110,height:100}}/> : 
                            item.subject_name == 'Bahasa Inggeris' ? <Image source={require("../images/subject_english.jpg")} style={{width:110,height:100}}/> :
                            item.subject_name == 'Matematik' ? <Image source={require("../images/subject_math.jpg")} style={{width:110,height:100}}/> :
                            item.subject_name == 'Sains' ? <Image source={require("../images/subject_science.jpg")} style={{width:110,height:100}}/> :
                            null
                        }
                    </View>
                    <View style={{padding:15, width:'70%'}}>
                      <Text style={{}}>{item.subject_name}</Text>
                      <Text style={{paddingTop:10}}>
                        {item.subjective_total_session} Session</Text>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
                        <View style={{ width: "100%", alignItems: "center" }}>
                          {this.state.dataSource[index].isSelected == true
                            ? this.renderDetails(
                                this.state.sessionList[
                                  index
                                ],
                                item.subject_id
                              )
                            : null}
                        </View>
                        <View />
                      </View>
                )}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    alignItems: "center"
  },
  backgroundImage: {
    position: "relative",
    top: 0,
    left: 0,
    justifyContent: "center",
    resizeMode: "cover",
    width: "100%",
    height: 200,
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden"
  },

  iconPracticeCircle: {
    height: 120,
    width: 120,
    overflow: "hidden",
    backgroundColor: "white",
    borderRadius: 120 / 2,
    borderWidth: 1,
    borderColor: "purple"
  },
  FlatListItemStyle: {
    padding: 30
  },
  subjectListCard: {
    width: "95%",
    backgroundColor: "#fff",
    elevation: 5,
    marginTop: 10,
    alignItems: "center"
  },
  practiceCard: {
    width: "100%",
    width: "95%",
    backgroundColor: "white",
    elevation: 10,
    marginTop: 10
  }
});

SubjectiveReport2.propTypes = propTypes;
export default SubjectiveReport2;

import React, { Component } from "react";
import { View, Text, StyleSheet, FlatList, ScrollView } from "react-native";
import {Actions} from "react-native-router-flux";
import HTML from "react-native-render-html";

class SubjectiveScoreAnswered extends Component {
    subjectiveReview(){
        Actions.subjectiveReview();
    }

    state = {};

  getScoreAnswered = (obj) => {
      console.log("nak tau obj dalam scoreanswered")
      console.log(obj)
      return (
          <ScrollView>
        <View style={styles.container}>
        <View style={{width:'100%', height:'100%', alignContent:'center', alignItems:'center'}}>
            <FlatList 
                data={obj}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => (
                <View style={styles.cardContainer}>
                <View style={{padding: 20,}}>
                    <View style={{flexDirection:'row', width:'100%'}}>
                        <View style={{width:'5%'}}>
                            <Text>
                                {item.question_no}
                            </Text>
                        </View>
                        <View style={{width:'95%'}}>
                        <HTML html={item.question_text} />
                        </View>
                    </View>

                    <View style={{flexDirection:'row',width:'100%', marginTop:10}}>
                        <View style={{width:'10%'}}>
                            <Text style={{color:'green'}}>
                                Ans:
                            </Text>
                        </View>
                        <View style={{width:'90%'}}>
                                { item.answer[0].answer_status == true ? <HTML html={item.answer[0].answer_text} /> : null}
                        </View>
                    </View>

                    <View style={{flexDirection:'row',width:'100%', marginTop:10}}>
                        <View style={{width:'25%'}}>
                        <View>
                                <View style={{borderWidth:2, borderColor:'cyan', borderRadius:15, alignItems:'center'}}> 
                                    <Text style={{padding:10}}>
                                        {item.difficulty_level}
                                    </Text>
                                </View>
                                </View>
                        </View>
                        <View style={{width:'75%', alignItems:'flex-end'}}>
                            <Text style={{color:"purple", paddingTop:10}} onPress={() => this.subjectiveReview(obj)}>
                                REVIEW
                            </Text>
                        </View>
                    </View>
                </View>
            </View> 
                )}
            />
            </View>
      </View>
      </ScrollView>
    );
    
}
  render() {
    return null
  }

//   state = {};
//   render() {
//     return (
//       <View style={styles.container}>
//         <ScrollView style={{width:'100%'}}>
//         <View style={{alignItems:'center', marginTop:10, marginBottom:10}}>
//             <View style={styles.cardContainer}>
//                 <View style={{padding:10, borderBottomWidth:1, borderColor:'silver'}}>
//                     <View style={{flexDirection:'row', width:'100%'}}>
//                         <View style={{width:'5%'}}>
//                             <Text>
//                                 1
//                             </Text>
//                         </View>
//                         <View style={{width:'95%'}}>
//                             <Text>
//                             Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
//                             Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
//                              when an unknown printer took a galley of type and scrambled it to make a 
//                              type specimen book.
//                             </Text>
//                         </View>
//                     </View>

//                     <View style={{flexDirection:'row',width:'100%', marginTop:10}}>
//                         <View style={{width:'5%'}}>
//                             <Text style={{color:'green'}}>
//                                 Ans:
//                             </Text>
//                         </View>
//                         <View style={{width:'95%'}}>
//                             <Text>
//                                 lorem ipsim
//                             </Text>
//                         </View>
//                     </View>

//                     <View style={{flexDirection:'row',width:'100%', marginTop:20}}>
//                         <View style={{width:'50%'}}>
//                             <View style={{borderWidth:2, borderColor:'#00c1df', borderRadius:10, width:'20%'}}> 
//                                 <View style={{padding: 10, alignItems:'center'}}>
//                                 <Text style={{color:'#00c1df'}}>
//                                     Easy   
//                                 </Text>
//                                 </View>
//                             </View>
//                         </View>
//                         <View style={{width:'50%', alignItems:'flex-end', justifyContent:'center'}}>
//                             <Text style={{color:"purple"}} onPress={this.subjectiveReview}>
//                                 REVIEW
//                             </Text>
//                         </View>
//                     </View>
//                 </View>

//                 <View style={{padding:10, borderBottomWidth:1, borderColor:'silver'}}>
//                     <View style={{flexDirection:'row', width:'100%'}}>
//                         <View style={{width:'5%'}}>
//                             <Text>
//                                 1
//                             </Text>
//                         </View>
//                         <View style={{width:'95%'}}>
//                             <Text>
//                             Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
//                             Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
//                              when an unknown printer took a galley of type and scrambled it to make a 
//                              type specimen book.
//                             </Text>
//                         </View>
//                     </View>

//                     <View style={{flexDirection:'row',width:'100%', marginTop:10}}>
//                         <View style={{width:'5%'}}>
//                             <Text style={{color:'green'}}>
//                                 Ans:
//                             </Text>
//                         </View>
//                         <View style={{width:'95%'}}>
//                             <Text>
//                                 lorem ipsim
//                             </Text>
//                         </View>
//                     </View>

//                     <View style={{flexDirection:'row',width:'100%', marginTop:20}}>
//                         <View style={{width:'50%'}}>
//                             <View style={{borderWidth:2, borderColor:'#00c1df', borderRadius:10, width:'20%'}}> 
//                                 <View style={{padding: 10, alignItems:'center'}}>
//                                 <Text style={{color:'#00c1df'}}>
//                                     Easy   
//                                 </Text>
//                                 </View>
//                             </View>
//                         </View>
//                         <View style={{width:'50%', alignItems:'flex-end', justifyContent:'center'}}>
//                             <Text style={{color:"purple"}}>
//                                 REVIEW
//                             </Text>
//                         </View>
//                     </View>
//                 </View>
//             </View>
//             </View>
//         </ScrollView>
//       </View>
//     );
//   }
}

const styles = StyleSheet.create({
    container:{
        width:'100%',
        alignItems: 'center',
    },
    cardContainer:{
        width:'95%',
        backgroundColor:"white",
        elevation:10
    }
})
export default SubjectiveScoreAnswered;

import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableWithoutFeedback,
  ScrollView,
  TouchableOpacity,
  Alert
} from "react-native";
import PropTypes from "prop-types";
import { images } from "../components";

const propTypes = {
  item: PropTypes.object
};

class SubjectiveItem extends Component {
  state = {
    isSelected: false
  };

  onPressHandler = (id) => {
    console.log(id)
    this.setState((prevState, prevProps) => ({
      isSelected: !prevState.isSelected
    }));
  };

  renderDetails = () => (
    <View style={{ width: "90%", elevation: 2, backgroundColor: "white" }}>
      <View>
        <Text style={styles.description}>Description</Text>
      </View>
    </View>
  );

  render() {
    const { isSelected } = this.state;
    return (

      <View style={{ width: "100%", elevation: 10 , marginTop:10, borderRadius:2, marginBottom:10}}>
        <View style={{ alignItems: "center" }}>
          <View style={{ width: "95%", backgroundColor: "white" , alignItems: 'center',}}>
          <View style={{width:'90%', alignItems: 'center', paddingTop:20, paddingBottom:10, borderBottomWidth: 0.5, borderColor:'green'}}>
            <Text>{this.props.item.name}</Text>
          </View>
          <View style={{flexDirection:'row',width:'90%',paddingTop:10, paddingBottom:10}}>
         
              <View style={{ width: "50%", backgroundColor: "white" }}>
                  <Text style={styles.title}>Topic</Text>
                  {console.log(this.props.item.id)}
              </View>
              <TouchableWithoutFeedback onPress={()=>this.onPressHandler(this.props.item.id)}>
              <View style={{ width: "50%", backgroundColor: "white", alignItems:'flex-end' }}>
              <Image source={images.expand} style={styles.image} />
              </View>
          </TouchableWithoutFeedback>
        
        </View>
        <View style={{ width: "100%", alignItems: "center" }}>
            {isSelected && this.renderDetails()}
          </View>
            <View />
          </View>
        </View>
      </View>
    );
  }
}

SubjectiveItem.propTypes = propTypes;

const styles = StyleSheet.create({
  titleContainer: {
    flexDirection: "row",
    justifyContent: "flex-end"
  },
  image: {
    height: 20,
    width: 20
  },
  title: {
    flex: 1
  },
  description: {}
});

export { SubjectiveItem };

import {AsyncStorage,Alert} from 'react-native';
import {setStore} from './Utilities';

//save data parent profile
// export const setStore = async (key, value) => {
//     value = JSON.stringify(value);
// 	try {
// 	  if (value) return AsyncStorage.setItem(key, value)
// 	} catch (error) {
// 	  // Error retrieving data
// 	  console.log(error.message);
// 	}
// };


//get data from live database
export const getDataSubscription = (uid) => {
  // Alert.alert(uid);
  // const { user_id } = uid;

  if (uid != "") {
    fetch(
      `http://api.hometutor.com.my/mobile_api/index.php?process=subscription_info&user_id=${uid}`,
      {
        method: "POST",
        headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
        },
        body: JSON.stringify({})
      }
    )
    .then(response => response.json())
    .then(responseJson => {
      var itemData = responseJson.items.subscription;
      
      if(itemData != null){
        setStore("activationcode", itemData.activationcode);
        setStore("activated_date", itemData.activated_date);
        setStore("expired_date", itemData.expired_date);
      } 
    })
    .catch(error => {
      console.error(error);
    });
  } else {
    Alert.alert("Please filled the field");
  }
};
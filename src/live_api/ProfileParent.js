import {AsyncStorage,Alert} from 'react-native';


//save data parent profile
export const saveParentProfile = async (key, value) => {
	try {
    value = JSON.stringify(value);
	  if (value) return AsyncStorage.setItem(key, value)
	} catch (error) {
	  // Error retrieving data
	  console.log(error.message);
	}
};


//get data from live database
export const getData = (uid) => {
  const { user_id } = uid;

  if (user_id != "") {
    fetch(
      `http://api.hometutor.com.my/mobile_api/index.php?process=profile_parent&user_id=${user_id}`,
      {
        method: "POST",
        headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
        },
        body: JSON.stringify({})
      }
    )
    .then(response => response.json())
    .then(responseJson => {
      var itemData = responseJson.items.profile;
      
      if(itemData != null){
        saveParentProfile("pp_fullname", itemData.fullname);
        saveParentProfile("pp_screenname", itemData.screen_name);
        saveParentProfile("pp_spousename", itemData.spouse_name);
        saveParentProfile("pp_icno", itemData.ic_number);
        saveParentProfile("pp_contact_no", itemData.contact_no);
        saveParentProfile("pp_home_no", itemData.home_no);
        saveParentProfile("pp_office_no", itemData.office_no);
        saveParentProfile("pp_fax_no", itemData.fax_no);
        saveParentProfile("pp_gender", itemData.gender);
        saveParentProfile("pp_dob", itemData.date_of_birth);
        saveParentProfile("pp_nationality", itemData.nationality);
        saveParentProfile("pp_address", itemData.address);
        saveParentProfile("pp_address_two", itemData.address_two);
        saveParentProfile("pp_city", itemData.city);
        saveParentProfile("pp_state", itemData.state);
        saveParentProfile("pp_country", itemData.country);
        saveParentProfile("pp_postcode", itemData.postcode);
        saveParentProfile("pp_username", itemData.username);
        saveParentProfile("pp_email", itemData.email);
      } 
    })
    .catch(error => {
      console.error(error);
    });
  } else {
    Alert.alert("Please filled the field");
  }
};
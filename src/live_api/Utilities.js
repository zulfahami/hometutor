import {AsyncStorage} from 'react-native';

// saveUserId = async (key, value) => {
//     try {
//        value = JSON.stringify(value);
//     //   AsyncStorage.setItem(key, temp);
//       if (value) return AsyncStorage.setItem(key, value)

//     } catch (error) {
//       // Error retrieving data
//       console.log(error.message);
//     }
//   };

export const setStore = async (key, value) => {
	try {
    value = JSON.stringify(value);
	  if (value) return AsyncStorage.setItem(key, value)
	} catch (error) {
	  // Error retrieving data
	  console.log(error.message);
	}
};

export const getStore = async key => {
  var findData = "";
  try {
    // console.log('test');
    findData = (await AsyncStorage.getItem(key)) || "none";
    // console.log(key+"-"+findData);
    return findData;
  } catch (error) {
    // Error retrieving data
    console.log(error.message);
  }
};

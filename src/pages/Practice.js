import React, { Component } from "react";
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  ScrollView,
  Alert,
  ActivityIndicator,
  FlatList,
  TouchableOpacity
} from "react-native";
import { PracticeItem } from "../components/PracticeItem";
import { getStore } from "../live_api/Utilities";
import { images } from "../components";
import PropTypes from "prop-types";
import { Actions } from "react-native-router-flux";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

const propTypes = {
  item: PropTypes.object
};

class Practice extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      level_id: "",
      subject_list: "",
      user_id: "",
      isSelected: false,
      topicList: [],
      level_name: ""
    };
  }

  subtopicList(item) {
    // item = JSON.stringify(item);
    // Alert.alert(item.topic_id);
    console.log(item.item.topic_id);
    console.log("nak tau item.item")
    console.log(item.item)

    fetch(`http://api.hometutor.com.my/mobile_api/index.php?process=subtopic_list&topic_id=${item.item.topic_id}&module=practice`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({})
    })
      .then(res => res.json())
      .then(responseJson => {
        // this.setState({
        //   topicList: this.state.topicList.concat([responseJson.items.topic])
        // });

        // if (--limit) {
        //   console.log(limit + " : " + responseJson);

        //   return this.doRecursiveRequestFunction(
        //     url,
        //     limit,
        //     subject_list,
        //     level_id
        //   );
        // }
        // console.log(this.state.topicList);
        // return responseJson;
        // console.log(item.item);
        // console.log(responseJson.items);
        let dataToUse = {data:{topic:item.item,subtopic:responseJson.items.subtopic}};
        console.log(dataToUse);
        Actions.subtopicPractice(dataToUse);
      })
      .catch(error => console.log(error));

  }

  questionInstruction(item) {
    console.log(item);
    Actions.questionInstruction(item);
  }

  doRecursiveRequestFunction = (url, limit, subject_list, level_id) => {
    console.log("this is string : " + JSON.stringify(subject_list[limit - 1]));
    var subjectIDTemp = JSON.stringify(subject_list[limit - 1].id);
    var urlExtra = `&subject_id=${subjectIDTemp}&level_id=${level_id}`;

    var fullUrl = url + urlExtra;

    console.log("This url : " + fullUrl);

    fetch(fullUrl, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({})
    })
      .then(res => res.json())
      .then(responseJson => {
        if(responseJson.items.topic != []){
        this.setState({
          topicList: this.state.topicList.concat([responseJson.items.topic])
        });
      }

        if (--limit) {
          console.log(limit + " : " + responseJson);

          return this.doRecursiveRequestFunction(
            url,
            limit,
            subject_list,
            level_id
          );
        }
        console.log(this.state.topicList);
        return responseJson;
      })
      .catch(error => console.log(error));
  };

  componentDidMount() {
    getStore("subject_list").then(value => {
      this.setState({
        isLoading: false,
        dataSource: JSON.parse(value)
      });
    });

    getStore("chosen_level_name").then(value => {
      this.setState({
        level_name: JSON.parse(value)
      });
    });

    console.log("in did mount");
    getStore("subject_list")
      .then(value => {
        this.setState({ subject_list: JSON.parse(value) });
      })
      .then(value => {
        getStore("level_id")
          .then(value => {
            this.setState({ level_id: JSON.parse(value) });
            console.log("levelsss_" + JSON.parse(value));
          })
          .then(value => {
            console.log("subject_list : " + this.state.subject_list);
            console.log("subject_size : " + this.state.subject_list.length);



            let i;
            let levelID = this.state.level_id;
            let url = `http://api.hometutor.com.my/mobile_api/index.php?process=topic&question_type=1`;

let levelTemp = 0;
            getStore("chosen_level_id")
          .then(value => {
            console.log("uuuuuuuu" + JSON.parse(value));
            levelTemp = JSON.parse(value);

            if(levelTemp != 0){
              levelID = levelTemp
            }
            console.log('lllll'+levelID);
            
                        this.doRecursiveRequestFunction(
                          url,
                          this.state.subject_list.length,
                          this.state.subject_list,
                          levelID
                        );
                      


          }).catch(err => {
            console.log(err);
          });
// if(levelTemp != 0){
//   levelID = levelTemp
// }
// console.log('lllll'+levelID);

//             this.doRecursiveRequestFunction(
//               url,
//               this.state.subject_list.length,
//               this.state.subject_list,
//               levelID
//             );
          })
          .catch(err => {
            console.log(err);
          });
      });
  }

  onPressHandler = id => {
    console.log(id);
    this.state.dataSource.map(item => {
      if (item.id == id) {
        item.isSelected == undefined
          ? (item.isSelected = true)
          : item.isSelected == true
          ? (item.isSelected = false)
          : item.isSelected == false
          ? (item.isSelected = true)
          : null;
      }
    });
    this.setState({ ...this.state });
    console.log(this.state.dataSource);
    // this.setState((prevState, prevProps) => ({
    //   ...this.state,
    //   isSelected: !prevState.isSelected
    // }));
    // console.log(this.state.isSelected)
  };

  //flatlist topic
  renderDetails = (topicList, subjectId, subjectName) => {
    console.log("suuuuuuubjectid : " + subjectId)
    console.log(subjectName)
    return (
      <View style={{ width: "90%" }}>
      
        <FlatList
          data={JSON.parse(JSON.stringify(topicList))}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => (
            <View
              style={{ flexDirection: "row", paddingBottom: 10, width: "100%" }}
            >
              <View style={{ width: "15%" }}>
                <Image
                  source={require("../images/icon_file_in_box.png")}
                  style={{ height: 45, width: 45 }}
                />
              </View>
              <View style={{ paddingLeft: 20, width: "65%" , justifyContent:'center' }}>
                <Text style={styles.description}>{item.name}</Text>
                {console.log("nak tau item topiclist")}
                {console.log(topicList)}
                {console.log("this is item : " + item)}
                {console.log(item.name)}
                {/* <Text>10 total question answered</Text> */}
              </View>

              {/* <View style={{ alignItems: "center", width: "10%" }}>
                <TouchableOpacity
                  onPress={() =>
                    this.questionInstruction({
                      item: {
                        topic_id: item.id,
                        level_id: 10,
                        subject_id: subjectId,
                        topic_name: item.name
                      }
                    })
                  }
                >
                  <View
                    style={{
                      paddingTop: 6,
                      paddingBottom: 6,
                      borderWidth: 1,
                      borderRadius: 3,
                      borderColor: "green",
                      paddingLeft: 10,
                      paddingRight: 10
                    }}
                  >
                    <Text style={{ color: "green" }}>Start</Text>
                  </View>
                </TouchableOpacity>
              </View> */}

              <View style={{ alignItems: "flex-end", width: "20%", justifyContent:'center' }}>
                <TouchableOpacity
                  onPress={() =>
                    this.subtopicList({
                      item: {
                        topic_id: item.id,
                        level_id: this.state.level_id,
                        subject_id: subjectId,
                        topic_name: item.name,
                        subject_name: subjectName
                      }
                    })
                  }
                >
                  <View>
                    <MaterialCommunityIcons name="chevron-right-circle" size={30} color="#40a339" />
                  </View>
                </TouchableOpacity>
              </View>

            </View>
          )}
        />
      </View>
    );
  };
  //flagtlist subject
  render() {
    const { isSelected } = this.state;
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, paddingTop: 20 }}>
          <ActivityIndicator />
        </View>
      );
    }
    return (
      <SafeAreaView>
        <ScrollView>
          <View style={{}}>
            <View style={styles.backgroundImage}>
              <Image source={require("../images/bg_blue.jpg")} />
              <View
                style={{
                  position: "absolute",
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                <View style={styles.iconPracticeCircle}>
                  <Image
                    source={require("../images/header_icon_exercise.jpg")}
                    style={{ width: 120, height: 120 }}
                  />
                </View>
                <View style={{ marginTop: 10 }}>
                  <Text style={{ color: "white", fontSize: 28 }}>Practice</Text>
                  <Text style={{ color: "white", fontSize: 15, textAlign:"center" }}>{this.state.level_name}</Text>
                </View>
              </View>
            </View>
            <View>
              <FlatList
                data={this.state.dataSource}
                extraData={this.state}
                renderItem={({ item, index }) => (
                  <View
                    style={{
                      width: "100%",
                      elevation: 10,
                      marginTop: 10,
                      borderRadius: 2,
                      marginBottom: 10
                    }}
                  >
                    <View style={{ alignItems: "center" }}>
                      <View
                        style={{
                          width: "95%",
                          backgroundColor: "white",
                          alignItems: "center"
                        }}
                      >
                        <View
                          style={{
                            width: "90%",
                            alignItems: "center",
                            paddingTop: 20,
                            paddingBottom: 10,
                            borderBottomWidth: 0.5,
                            borderColor: "green",
                          }}
                        >
                          <Text>{item.name}</Text>
                        </View>
                        <TouchableOpacity
                          onPress={() => this.onPressHandler(item.id)}
                        >
                          <View
                            style={{
                              flexDirection: "row",
                              width: "90%",
                              paddingTop: 10,
                              paddingBottom: 10
                            }}
                          >
                            <View
                              style={{ width: "50%", backgroundColor: "white" }}
                            >
                              <Text style={styles.title}>Topic</Text>
                            </View>

                            <View
                              style={{
                                width: "50%",
                                backgroundColor: "white",
                                alignItems: "flex-end"
                              }}
                            >
                              {this.state.isSelected == false ? (
                                <Image
                                  source={require("../images/ic_arrow_down.png")}
                                />
                              ) : (
                                <Image
                                  source={require("../images/ic_arrow_up.png")}
                                />
                              )}
                              {console.log(this.state.isSelected)}
                            </View>
                          </View>
                        </TouchableOpacity>
                        <View style={{ width: "100%", alignItems: "center" }}>
                          {/* {console.log("subjectid : "+item.id)} */}
                          {item.isSelected == true
                            ? this.renderDetails(
                                this.state.topicList[
                                  this.state.dataSource.length - 1 - index
                                ],
                                item.id,item.name
                              )
                            : null}
                        </View>
                        <View />
                      </View>
                    </View>
                  </View>
                )}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    position: "relative",
    top: 0,
    left: 0,
    justifyContent: "center",
    resizeMode: "cover",
    width: "100%",
    height: 200,
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden"
  },

  iconPracticeCircle: {
    height: 120,
    width: 120,
    overflow: "hidden",
    backgroundColor: "white",
    borderRadius: 120 / 2,
    borderWidth: 1,
    borderColor: "purple"
  },
  FlatListItemStyle: {
    padding: 30
  },
  subjectListCard: {
    width: "95%",
    backgroundColor: "#fff",
    elevation: 5,
    marginTop: 10,
    alignItems: "center"
  }
});

Practice.propTypes = propTypes;
export default Practice;
//onPress={this.GetFlatListItem.bind(this, item.subject_list)}

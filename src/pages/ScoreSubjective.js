import React, { Component } from "react";
import { View, Text, StyleSheet, ScrollView, Image, SafeAreaView, Dimensions, ActivityIndicator } from "react-native";
import { TabView, TabBar, SceneMap, Route, NavigationState } from "react-native-tab-view";

import Score_Answered from "../components/SubjectiveScoreAnswered";
import Score_Skipped from "../components/SubjectiveScoreSkipped";

import { setStore, getStore } from "../live_api/Utilities";

// import { Actions } from "react-native-router-flux";

const initialLayout = {
    height: 0,
    width: Dimensions.get("window").width
  };

class ScoreSubjective extends Component {

    state = {
        index: 0,
        routes: [
          { key: "answered", title: "Answered", page: null, scoreCount: 0 },
          { key: "skipped", title: "Skipped", page: null, scoreCount: 0 }
        ]
      };

    _handleIndexChange = index =>
    this.setState({
      index
    });

  _renderTabBar = props => (
    <TabBar
      {...props}
      indicatorStyle={styles.indicator}
      style={styles.tabbar}
      tabStyle={styles.tab}
      labelStyle={styles.label}
    />
  );

  componentDidMount() {

    getStore("question_session").then(value => {
      let dataQuestion = JSON.parse(value);
      
      let total_not_answered = [];
      let total_answered = [];

      for (var i = 0; i < dataQuestion.length; i++) {
        if (dataQuestion[i].questionData.answered_status == 0) {
          total_not_answered = total_not_answered.concat(dataQuestion[i].questionData);
        } else {
          total_answered = total_answered.concat(dataQuestion[i].questionData);
        }
      }

      const scoreNotAnswered = new Score_Skipped();
      const scoreAnswered = new Score_Answered();

      this.state.routes[0].page = () => scoreAnswered.getScoreAnswered(total_answered);
      this.state.routes[1].page = () => scoreNotAnswered.getScoreSkipped(total_not_answered)

      this.state.routes[0].title =  this.state.routes[0].title + ' ' + total_answered.length;
      this.state.routes[1].title =  this.state.routes[1].title + ' ' + total_not_answered.length;

      this.setState({
        ...this.state
      });

    });
  }

  circleContainer = (total) => {
    return (<View><Text style={{color:'yellow'}}>{total}</Text></View>)
  }


  // _renderScene = SceneMap({
  //   answered: Score_Answered,
  //   skipped: Score_Skipped
  
  // });

  renderTabTab = () => {
    return _renderScene = SceneMap({
      answered: this.state.routes[0].page,
      skipped: this.state.routes[1].page
      });
    };
    
  render() {
    if (this.state.routes[0].page == null) {
      return (
        <View style={{ flex: 1, paddingTop: 20 }}>
          <ActivityIndicator />
        </View>
      );
    } else {
    return (
        <SafeAreaView>
                    <View style={{alignItem:'center', width:'100%', height:'100%'}}>
                        <View style={styles.backgroundImage}>  
                            <Image source={require("../images/bg_green_bananaleaf.jpg")} />
                            <View
                                style={{
                                position: "absolute",
                                alignItems: "center",
                                justifyContent: "center"
                                }}
                            >
                                <View style={styles.iconSummaryCircle}>
                                    <Image
                                        source={require("../images/pencil_icon.png")}
                                        style={{ width: 120, height: 120 }}
                                    />
                                </View>
                                <View style={{ marginTop: 10 }}>
                                    <Text style={{ color: "white", fontSize: 28 }}>Score</Text>
                                </View>
                            </View>
                        </View>
                       
                        <View style={{width:'100%',height:'70%', paddingBottom:10}}>
                            <TabView
                            style={[styles.container, this.props.style]}
                            navigationState={this.state}
                            renderScene={this.renderTabTab()}
                            renderTabBar={this._renderTabBar}
                            onIndexChange={this._handleIndexChange}
                            initialLayout={initialLayout}
                                />
                        </View>
                    </View>
                  
            </SafeAreaView>
    );
                              }
  }
}

const styles = StyleSheet.create({
    backgroundImage: {
      position: "relative",
      top: 0,
      left: 0,
      justifyContent: "center",
      resizeMode: "cover",
      width: "100%",
      height: 200,
      justifyContent: "center",
      alignItems: "center",
      overflow: "hidden"
    },
  
    iconSummaryCircle: {
      height: 120,
      width: 120,
      overflow: "hidden",
      backgroundColor: "white",
      borderRadius: 120 / 2,
      borderWidth: 1,
      borderColor: "purple"
    },
    tabbar: {
      backgroundColor: "#6967fb"
    },
    tab: { 
      
    },
    indicator: {
      backgroundColor: "cyan"
    },
    label: {
      color:"#fff",
      fontWeight: "bold"
    }
  });


export default ScoreSubjective;

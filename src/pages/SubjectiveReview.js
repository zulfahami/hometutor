import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Image,
  Dimensions,
  ActivityIndicator
} from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import ReactNativeItemSelect from "react-native-item-select";
import { TabView, TabBar, SceneMap } from "react-native-tab-view";
import HTML from 'react-native-render-html';

import { setStore, getStore } from "../live_api/Utilities";

const initialLayout = {
  height: 0,
  width: Dimensions.get("window").width
};

// const data = [
//   { firstLetter: "A", displayName: "Two" },
//   { firstLetter: "B", displayName: "One" },
//   { firstLetter: "C", displayName: "Three" },
//   { firstLetter: "D", displayName: "Unknown" }
// ];



class SubjectiveReview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      isLoading: true,
      routes: [
        { key: "1", title: "1", page: null },
        { key: "2", title: "2", page: null },
        { key: "3", title: "3", page: null },
        { key: "4", title: "4", page: null },
        { key: "5", title: "5", page: null }
      ]
    };
  }

  static title = "Scrollable top bar";
  static backgroundColor = "#3f51b5";
  static appbarElevation = 0;

  // state = {
  //   index: 0,
  //   routes: [
  //     { key: "progress", title: "Progress" },
  //     { key: "performance", title: "Performance" }
  //   ]
  // };

  // componentDidMount() {
    // this.state.routes.map(item => {
    //   let obj = {};
    //   obj.name = item.title;
    //   const pages = () => RoutePage(obj);
    //   item.page = pages;
    // });
    // this.setState({
    //   isLoading: false,
    //   ...this.state
    // });
  // }

  RoutePage (obj) {
    if (obj == undefined) {
      obj = {};
      obj.name = "Fakhri";
    }
    console.log('in orute');
    console.log(obj.key);
  
    return (
      <View style={{ width: "100%", alignItems: "center" }}>
        <View style={styles.cardContainer}>
          <View style={{ flexDirection: "row", width: "100%", padding: 10 }}>
            <View style={{ width: "90%" }}>
              <Text>{obj.dataQuestion2.questionData.question_code_parent == "" ? obj.dataQuestion2.questionData.question_code : obj.dataQuestion2.questionData.question_code_parent+" - "+obj.dataQuestion2.questionData.question_code}</Text>
            </View>
          </View>
          <ScrollView>
            <View style={{ paddingLeft: 10, paddingRight: 10, paddngBottom: 20 }}>
              <View style={{ paddingBottom: 20 }}>
                <Text style={{ fontWeight: "bold" }}>
                  Baca petikan secara mekanis
                </Text>
              </View>
              <View>
                <ScrollView horizontal={true}>
              {obj.dataQuestion2.questionData.question_image.path != null &&obj.dataQuestion2.questionData.question_image.path != "" ?
             <Image
             source={{ uri: obj.dataQuestion2.questionData.question_image.path }}
             width={obj.dataQuestion2.questionData.question_image.path.width}
             // style={{ maxWidth:'100%',width:obj.question_image_parent.width}}
           />: null }
                <HTML html={obj.dataQuestion2.questionData.question_text}/>
                </ScrollView>
              </View>
            </View>
            <View style={{ padding: 10 }}>
              <Text>ANSWER</Text>
              <View
                style={{ borderWidth: 1, borderColor: "silver", borderRadius: 4 }}
              >
                <View style={{ padding: 10 }}>   
                  <HTML html= {obj.dataQuestion2.questionData.answer[0].answer_text} />
                  <View style={{ padding: 10 }}>
                    <View style={{paddingBottom:10}}>
                      <Text style={{ color: "green" }}>Sample Answer</Text>
                    </View>
                    <View>
                      <HTML html={obj.dataQuestion2.questionData.answer[0].sampleanswer}/>
                    </View>
                  </View>
  
                  <View style={{ padding: 10 }}>
                    <View style={{paddingBottom:10}}>
                      <Text style={{ color: "green" }}>Reason</Text>
                    </View>
                    <View>
                     <HTML html= {obj.dataQuestion2.questionData.answer[0].reason}/>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    );
    };
    

  componentDidMount() {
    console.log(this.props.obj)

    getStore("question_session").then(value => { 
      let dataQuestion = JSON.parse(value);  
      console.log(dataQuestion);
      console.log("nak tengok state routes")
      console.log(this.state.routes);
      
      count = -1;
      this.state.routes.map(item => {
        count++;
        let obj = {};
        obj.name = item.title;
      
        const pages = () => this.RoutePage(item);
      
        item.page = pages;
        item.dataQuestion2 = dataQuestion[count];
        obj.data = item.dataQuestion2;

        this.setState({
          route: dataQuestion,
          isLoading: false,
          ...this.state
        });
      });
      
    }); //get id for api
    
  }



  _handleIndexChange = index =>
    this.setState({
      index
    });

  _renderTabBar = props => (
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={styles.indicator}
      style={styles.tabbar}
      tabStyle={styles.tab}
      labelStyle={styles.label}
    />
  );

  // _renderScene = SceneMap({
  //   1: Dashboard_Progress,
  //   2:Dashboard_Performance

  // });

  renderTabTab = () => {
    const obj = Object.assign(
      {},
      ...this.state.routes.map(item => {
        return { [item.key]: item.page };
      })
    );

    console.log(obj);
    // let obj = {

    //   1:this.state.routes[0].page,
    //   2:this.state.routes[1].page,
    //   3:this.state.routes[2].page,
    //   4:this.state.routes[3].page,
    //   5:this.state.routes[4].page
    //  }
    return (_renderScene = SceneMap(obj));
  };

  render() {
    if (this.state.routes[0].page == null) {
      return (
        <View style={{ flex: 1, paddingTop: 20 }}>
          <ActivityIndicator />
        </View>
      );
    } else {
      return (
        <View style={styles.container2}>
          <View style={{}}>
            <TabView
              style={[styles.container, this.props.style]}
              navigationState={this.state}
              renderScene={this.renderTabTab()}
              renderTabBar={this._renderTabBar}
              onIndexChange={this._handleIndexChange}
              initialLayout={initialLayout}
            />
          </View>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container2: {
    width: "100%",
    alignItems: "center",
    height: "100%"
  },
  cardContainer: {
    width: "95%",
    backgroundColor: "white",
    borderRadius: 2,
    elevation: 100,
    marginTop: 10
  },
  scene: {
    flex: 1
  },
  container: {
    flex: 1
  },
  tabbar: {
    backgroundColor: "#fff"
  },
  tab: {
    width: 50
  },
  indicator: {
    backgroundColor: "#6047ab"
  },
  label: {
    color: "black",
    fontWeight: "bold"
  }
});

export default SubjectiveReview;

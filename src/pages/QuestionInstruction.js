import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  Image,
  StyleSheet,
  TouchableOpacity
} from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { Actions } from "react-native-router-flux";
import { setStore, getStore } from "../live_api/Utilities";

class QuestionInstruction extends Component {
  questionPage(item) {
    console.log(item);
    
    getStore("profile_user_id").then(value => {
      let userId = value;
      console.log(`http://api.hometutor.com.my/mobile_api/index.php?process=practice_start_session&user_id=${userId}&subject_id=${item.subject_id}&topic_id=${item.topic_id}&subtopic_id=${item.subtopic_id}`);
      
      fetch(`http://api.hometutor.com.my/mobile_api/index.php?process=practice_start_session&user_id=${userId}&subject_id=${item.subject_id}&topic_id=${item.topic_id}&subtopic_id=${item.subtopic_id}`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({})
    })
      .then(res => res.json())
      .then(responseJson => {
        let statusStartPractice = responseJson.items.status;
        let practiceSessionId = responseJson.items.session_id;
        console.log(responseJson);
        if(statusStartPractice == 1){
          setStore("session_practice",{sess_id:practiceSessionId,topic_id:item.topic_id,subtopic_id:item.subtopic_id});
        }
      })
      .catch(error => console.log(error));
      

      Actions.questionPage(item);
    });

    
    
  }
  render() {
    return (
      <View style={styles.container}>
      
        <ScrollView style={{ height: "100%", width: "100%" }}>
          <View style={styles.backgroundImage}>
            <Image
              source={require("../images/bg_green.jpg")}
              style={{ width: "100%" }}
            />
            <View
              style={{
                position: "absolute",
                alignItems: "center",
                justifyContent: "center",
                flexDirection: "row",
                width: "100%"
              }}
            >
              <View style={{ width: "30%", alignItems: "center" }}>
                <Text style={{ color: "white" }}>5 Questions</Text>
              </View>
              <View style={{ width: "40%", alignItems: "center" }}>
                <View style={styles.imageProfileCircle}>
                  <Image
                    source={require("../images/pencil_icon.png")}
                    style={{ width: 150, height: 150 }}
                  />
                </View>
                <View style={{ paddingTop: 10 }}>
                  <Text style={{ color: "white" }}>{this.props.item.topic_name} - {this.props.item.subtopic_name}</Text>
                </View>
              </View>
              <View style={{ width: "30%", alignItems: "center" }}>
                <Text style={{ color: "white" }}>No times limit</Text>
              </View>
            </View>
          </View>
          <View style={{ alignItems: "center" }}>
            <View style={styles.instructionCard}>
              <View style={{ paddingTop: 10, alignItems: "center" }}>
                <Text style={{ fontSize: 24 }}>INSTRUCTIONS</Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  flex: 1,
                  flexWrap: "wrap",
                  paddingTop: 30
                }}
              >
                <View
                  style={{
                    width: "20%",
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <MaterialCommunityIcons
                    name="message-alert"
                    color="orange"
                    size={30}
                  />
                </View>

                <View style={{ width: "80%", paddingRight: 10 }}>
                  <Text style={{ textAlign: "left" }}>
                    This exercise contains multiple-choice or comprehensive
                    multiple-choice questions. No marks are awarded for
                    unattempted questions.
                  </Text>
                </View>
              </View>

              <View
                style={{
                  flexDirection: "row",
                  flex: 1,
                  flexWrap: "wrap",
                  paddingTop: 20
                }}
              >
                <View style={{ width: "20%", alignItems: "center" }}>
                  <MaterialCommunityIcons
                    name="spellcheck"
                    color="orange"
                    size={30}
                  />
                </View>

                <View style={{ width: "80%", paddingRight: 10 }}>
                  <Text style={{ textAlign: "left" }}>
                    Tap to select the correct answer and double tap to zoom in
                    any image.
                  </Text>
                </View>
              </View>

              <View
                style={{
                  flexDirection: "row",
                  flex: 1,
                  flexWrap: "wrap",
                  paddingTop: 20,
                  paddingBottom: 10
                }}
              >
                <View style={{ width: "20%", alignItems: "center" }}>
                  <MaterialCommunityIcons
                    name="alarm"
                    color="orange"
                    size={30}
                  />
                </View>

                <View style={{ width: "80%", paddingRight: 10 }}>
                  <Text style={{ textAlign: "left" }}>
                    This exercise module has 5 questions and no time limit
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>

        
          <View
            style={{ position: "absolute", right: 0, bottom: 0, padding: 10 }}
          ><TouchableOpacity onPress={()=>this.questionPage(this.props.item)}>
            <View style={styles.circleIcon}>
              <MaterialCommunityIcons
                name="chevron-right"
                color="white"
                size={24}
              />
            </View>
            </TouchableOpacity>
          </View>
     
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    alignItems: "center",
    position: "relative"
  },
  backgroundImage: {
    position: "relative",
    top: 0,
    left: 0,
    justifyContent: "center",
    resizeMode: "cover",
    width: "100%",
    height: 250,
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden"
  },
  imageProfileCircle: {
    height: 150,
    width: 150,
    overflow: "hidden",
    backgroundColor: "silver",
    borderRadius: 150 / 2,
    borderWidth: 1,
    borderColor: "purple",
    elevation: 10
  },
  instructionCard: {
    width: "95%",
    marginTop: 10,
    backgroundColor: "white",
    borderRadius: 2,
    alignItems: "center",
    elevation: 30,
    flex: 1,
    position: "relative",
    top: -50
  },
  circleIcon: {
    width: 70,
    height: 70,
    borderRadius: 70 / 2,
    backgroundColor: "#744acb",
    alignItems: "center",
    justifyContent: "center"
  }
});
export default QuestionInstruction;

import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Image,
  AsyncStorage,
  Alert
} from "react-native";
import { Button } from "react-native-elements";

import FadeInView from "../components/FadeInView";
import { Actions } from "react-native-router-flux";

const saveUserId = async (key, value) => {
  try {
    value = JSON.stringify(value);
    //   AsyncStorage.setItem(key, temp);
    if (value) return AsyncStorage.setItem(key, value);
  } catch (error) {
    // Error retrieving data
    console.log(error.message);
  }
};

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = { username: "", password: "" };
  }

  homemain = () => {
    let { username, password } = this.state;

    if (username != "" && password != "") {
      fetch(
        `http://api.hometutor.com.my/mobile_api/index.php?process=login&username=${username}&password=${password}`,
        {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
          },
          body: JSON.stringify({})
        }
      )
        .then(response => response.json())
        .then(responseJson => {
          var itemData = responseJson.items;

          var dataLogin = itemData.login;
          var dataActivation = itemData.activation;
          var dataPackage = itemData.packages;
          var dataLevel = itemData.level;
          var dataProfile = itemData.profile;
          var dataSubjectList = itemData.subject_list;
          var dataLevelList = itemData.level_list;

          saveUserId("all_login_data", itemData);

          saveUserId("login_status", dataLogin.status);
          saveUserId("login_message", dataLogin.message);
          if (dataLogin.status != 0) {
            saveUserId("activation_status", dataActivation.status);
            saveUserId("activation_start", dataActivation.activated_date);
            saveUserId("activation_end", dataActivation.expired_date);
            saveUserId("activation_message", dataActivation.message);
            saveUserId("package_id", dataPackage.id);
            saveUserId("package_name", dataPackage.name);
            saveUserId("level_id", dataLevel.id);
            saveUserId("level_name", dataLevel.name);

            saveUserId("chosen_level_id", dataLevel.id);
            saveUserId("chosen_level_name", dataLevel.name);

            saveUserId("profile_fullname", dataProfile.fullname);
            saveUserId("profile_user_id", dataProfile.user_id);
            saveUserId("profile_display_name", dataProfile.display_name);
            saveUserId("profile_username", dataProfile.username);
            saveUserId("profile_email", dataProfile.email);
            saveUserId("profile_gender", dataProfile.gender);
            saveUserId("profile_school_name", dataProfile.school_name);
            saveUserId("profile_school_level", dataProfile.school_level);
            saveUserId("profile_parent_id", dataProfile.parent_id);

            saveUserId("subject_list", dataSubjectList);
            saveUserId("level_list", dataLevelList).then(responsesKey => {
              Alert.alert(dataLogin.message);
              Actions.home();
            });
          } else {
            Alert.alert(dataLogin.message);
          }
        })
        .catch(error => {
          console.error(error);
        });
    } else {
      Alert.alert("Please filled the field");
    }
  };

  // home() {
  //     Actions.home()
  // }

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            position: "absolute",
            top: 0,
            left: 0,
            width: "100%",
            height: "100%"
          }}
        >
          <Image
            source={require("../images/bg_login.jpg")}
            style={{ resizeMode: "cover", width: "100%", height: "100%" }}
          />
        </View>
        <View style={{ width: "100%", alignItems: "center" }}>
          <View style={{ paddingTop: 40 }}>
            <Image
              source={require("../images/logo_hometutor.png")}
              style={{
                width: 150,
                height: 100,
                resizeMode: "contain"
              }}
            />
          </View>

          <View
            style={{
              backgroundColor: "#fff",
              width: "90%",
              elevation: 2,
              borderRadius: 2,
              paddingBottom: 20
            }}
          >
            <View style={{ alignItems: "center" }}>
              <View style={{ paddingTop: 24 }}>
                <Text style={{ fontSize: 18 }}>Welcome</Text>
              </View>
              <View>
                <Text
                  style={{
                    alignItems: "center",
                    textAlign: "center",
                    padding: 10
                  }}
                >
                  Good to see you. Please enter your username and password to
                  continue.
                </Text>
              </View>
            </View>
            <View style={{ padding: 18 }}>
              <View>
                <TextInput
                autoCapitalize='none'
                  style={styles.inputContainer}
                  placeholder="Username"
                  onChangeText={TextInputValue =>
                    this.setState({ username: TextInputValue })
                  }
                />
              </View>
              <View style={{ paddingTop: 10 }}>
                <TextInput
                secureTextEntry={true}
                password={true}
                autoCapitalize='none'
                  style={styles.inputContainer}
                  placeholder="Password"
                  onChangeText={TextInputValue =>
                    this.setState({ password: TextInputValue })
                  }
                />
              </View>
            </View>
            <View style={{ marginTop: 10 }}>
              <Button
                title="Login"
                backgroundColor="green"
                onPress={this.homemain}
                style={{ width: "100%" }}
              />
              <Text
                style={{ textAlign: "center", fontSize: 11, paddingTop: 5 }}
              >
                Please sign up first at www.hometutor.com.my
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#f2f2f2",
    flex: 1
  },

  inputContainer: {
    borderColor: "grey",
    borderWidth: 1,
    borderRadius: 4,
    padding: 10
  }
});

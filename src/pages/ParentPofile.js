import React, { Component } from "react";
import { View, Text, StyleSheet, ScrollView, Image, AsyncStorage, Alert } from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {getStore} from '../live_api/Utilities';
import {getData} from '../live_api/ProfileParent';

class ParentProfile extends Component {
	constructor(props) {
		super(props);
		this.state = {  
			user_id: "", pp_fullname: "", pp_screenname: "", pp_spousename: "", pp_icno: "", pp_contact_no: "",
			pp_home_no: "", pp_office_no: "", pp_fax_no: "", pp_gender: "", pp_dob: "", pp_nationality: "",
			pp_address: "", pp_address_two: "", pp_city: "", pp_state: "", pp_country: "", pp_postcode: "",
			pp_username: "", pp_email: ""
		};
	}

	componentDidMount() {
		getStore("profile_user_id").then(value => { this.setState({ user_id: JSON.parse(value) }); //get id for api
		}).then(value=>{ getData(this.state.user_id); //get data from api
		}).then(value=>{ getStore("pp_fullname").then(value => { this.setState({ pp_fullname: JSON.parse(value) }); })
		}).then(value=>{ getStore("pp_screenname").then(value => { this.setState({ pp_screenname: JSON.parse(value) }); })
		}).then(value=>{ getStore("pp_spousename").then(value => { this.setState({ pp_spousename: JSON.parse(value) }); })
		}).then(value=>{ getStore("pp_icno").then(value => { this.setState({ pp_icno: JSON.parse(value) }); })
		}).then(value=>{ getStore("pp_contact_no").then(value => { this.setState({ pp_contact_no: JSON.parse(value) }); })
		}).then(value=>{ getStore("pp_home_no").then(value => { this.setState({ pp_home_no: JSON.parse(value) }); })
		}).then(value=>{ getStore("pp_office_no").then(value => { this.setState({ pp_office_no: JSON.parse(value) }); })
		}).then(value=>{ getStore("pp_fax_no").then(value => { this.setState({ pp_fax_no: JSON.parse(value) }); })
		}).then(value=>{ getStore("pp_gender").then(value => { this.setState({ pp_gender: JSON.parse(value) }); })
		}).then(value=>{ getStore("pp_dob").then(value => { this.setState({ pp_dob: JSON.parse(value) }); })
		}).then(value=>{ getStore("pp_nationality").then(value => { this.setState({ pp_nationality: JSON.parse(value) }); })
		}).then(value=>{ getStore("pp_address").then(value => { this.setState({ pp_address: JSON.parse(value) }); })
		}).then(value=>{ getStore("pp_address_two").then(value => { this.setState({ pp_address_two: JSON.parse(value) }); })
		}).then(value=>{ getStore("pp_city").then(value => { this.setState({ pp_city: JSON.parse(value) }); })
		}).then(value=>{ getStore("pp_state").then(value => { this.setState({ pp_state: JSON.parse(value) }); })
		}).then(value=>{ getStore("pp_country").then(value => { this.setState({ pp_country: JSON.parse(value) }); })
		}).then(value=>{ getStore("pp_postcode").then(value => { this.setState({ pp_postcode: JSON.parse(value) }); })
		}).then(value=>{ getStore("pp_username").then(value => { this.setState({ pp_username: JSON.parse(value) }); })
		}).then(value=>{ getStore("pp_email").then(value => { this.setState({ pp_email: JSON.parse(value) }); })});
	}

  render() {
	return (
		<ScrollView>
	  <View style={styles.container}>
		<View style={styles.backgroundImage}>
		  <Image
			source={require("../images/bg_blue.jpg")}
			style={{ width: "100%", }}
		  />
		  <View
			style={{
			  position: "absolute",
			  alignItems: "center",
			  justifyContent: "center",
			  
			}}
		  >
			<View style={{}}>
			  <View style={styles.imageProfileCircle}>
				<Image
				  source={require("../images/default_user.png")}
				  style={{ width: 150, height: 150 }}
				/>
			  </View>
			  <View style={{ marginTop: 10 }}>
				<Text style={{ textAlign: "center", color: "white" }}>
				  Parent Profile
				</Text>
			  </View>
			</View>
		  </View>
		</View>
		 
		<View style={{width:'95%', backgroundColor:'white', elevation:5, marginTop:10, justifyContent:'center'}}>
			<View style={{padding:15, borderBottomWidth:0.5, borderColor:'silver', backgroundColor:'#fcfcfc'}}>
				<Text>
					Parent Account Information
				</Text>
			</View>

			<View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='account' size={26}/>
				</View>
				<View style={{justifyContent:'center',width:'80%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.pp_username}</Text>
					<Text>Parent Username</Text>
				</View>
			</View>

			<View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='email' size={26}/>
				</View>
				<View style={{justifyContent:'center',width:'80%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.pp_email}</Text>
					<Text>Email</Text>
				</View>
			</View>

		</View>

			<View style={{width:'95%', backgroundColor:'white', elevation:5, marginTop:10, justifyContent:'center'}}>
			<View style={{padding:15, borderBottomWidth:0.5, borderColor:'silver', backgroundColor:'#fcfcfc'}}>
				<Text>
					Parent Personal Information
				</Text>
			</View>

			<View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='account-outline' size={26}/>
				</View>
				<View style={{justifyContent:'center',width:'80%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.pp_fullname}</Text>
					<Text>Fullname</Text>
				</View>
			</View>

			 <View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='cast' size={26}/>
				</View>
				<View style={{justifyContent:'center',width:'80%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.pp_screenname}</Text>
					<Text>Screen Name</Text>
				</View>
			</View>

			 <View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='human-male-female' size={26}/>
				</View>
				<View style={{justifyContent:'center',width:'80%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.pp_gender}</Text>
					<Text>Gender</Text>
				</View>
			</View>

			<View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='account-multiple' size={26}/>
				</View>
				<View style={{justifyContent:'center',width:'80%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.pp_spousename}</Text>
					<Text>Spouse Name</Text>
				</View>
			</View>

			 <View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='account-card-details' size={26}/>
				</View>
				<View style={{justifyContent:'center',width:'80%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.pp_icno}</Text>
					<Text>IC Number</Text>
				</View>
			</View>

			 <View style={{flexDirection:'row', width:'100%', paddingBottom:10}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='calendar' size={26}/>
				</View>
				<View style={{justifyContent:'center',width:'80%',}}>
					<Text>{this.state.pp_dob}</Text>
					<Text>Date of birth</Text>
				</View>
			</View>
			</View>

			 <View style={{width:'95%', backgroundColor:'white', elevation:5, marginTop:10, justifyContent:'center',marginBottom:10}}>
			<View style={{padding:15, borderBottomWidth:0.5, borderColor:'silver', backgroundColor:'#fcfcfc'}}>
				<Text>
					Parent Contact Information
				</Text>
			</View>

			<View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='phone' size={26}/>
				</View>
				<View style={{justifyContent:'center',width:'80%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.pp_contact_no}</Text>
					<Text>Contact Number</Text>
				</View>
			</View>

			 <View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='phone-hangup' size={26}/>
				</View>
				<View style={{justifyContent:'center',width:'80%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.pp_home_no}</Text>
					<Text>Home phone number</Text>
				</View>
			</View>

			 <View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialIcons name='contact-phone' size={26}/>
				</View>
				<View style={{justifyContent:'center',width:'80%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.pp_office_no}</Text>
					<Text>Office phone number</Text>
				</View>
			</View>

			<View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
				<MaterialIcons name='contact-phone' size={26}/>
				</View>
				<View style={{justifyContent:'center',width:'80%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.pp_fax_no ? this.state.pp_fax_no : 'N/A'}</Text>
					<Text>Fax Number</Text>
				</View>
			</View>

			 <View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='home' size={26}/>
				</View>
				<View style={{justifyContent:'center',width:'80%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.pp_address}</Text>
					<Text>Address</Text>
				</View>
			</View>

			<View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='home' size={26}/>
				</View>
				<View style={{justifyContent:'center',width:'80%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.pp_address_two}</Text>
					<Text>Address two</Text>
				</View>
			</View>

			<View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='home' size={26}/>
				</View>
				<View style={{justifyContent:'center',width:'80%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.pp_postcode}</Text>
					<Text>Postcode</Text>
				</View>
			</View>

			<View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='home' size={26}/>
				</View>
				<View style={{justifyContent:'center',width:'80%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.pp_city}</Text>
					<Text>City</Text>
				</View>
			</View>

			<View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='home' size={26}/>
				</View>
				<View style={{justifyContent:'center',width:'80%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.pp_state}</Text>
					<Text>State</Text>
				</View>
			</View>

			<View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='home' size={26}/>
				</View>
				<View style={{justifyContent:'center',width:'80%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.pp_country}</Text>
					<Text>Country</Text>
				</View>
			</View>

			<View style={{flexDirection:'row', width:'100%',paddingBottom:10}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='home' size={26}/>
				</View>
				<View style={{justifyContent:'center',width:'80%',}}>
					<Text>{this.state.pp_nationality}</Text>
					<Text>Nationality</Text>
				</View>
			</View>
			</View>
	  </View>
	  </ScrollView>
	);
  }
}

const styles = StyleSheet.create({
	container:{
		width:'100%',
		alignItems: 'center',
	   
	},
  backgroundImage: {
	position: "relative",
	top: 0,
	left: 0,
	justifyContent: "center",
	resizeMode: "cover",
	width: "100%",
	height:250,
	justifyContent: "center",
	alignItems: "center",
	overflow: "hidden"
  },
  imageProfileCircle: {
	height: 150,
	width: 150,
	overflow: "hidden",
	backgroundColor: "silver",
	borderRadius: 150 / 2,
	borderWidth: 1,
	borderColor: "purple"
  },
});

export default ParentProfile;

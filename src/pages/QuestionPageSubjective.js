import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Dimensions,
  ActivityIndicator,
  TextInput,
  KeyboardAvoidingView,
  WebView
} from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import ReactNativeItemSelect from "react-native-item-select";
import { TabView, TabBar, SceneMap } from "react-native-tab-view";
import Dashboard_Progress from "./Dashboard_Progress";
import Dashboard_Performance from "./Dashboard_Performance";
import { setStore, getStore } from "../live_api/Utilities";
import { Actions } from "react-native-router-flux";
import Image from "react-native-scalable-image";
import HTML from "react-native-render-html";

const initialLayout = {
  height: 0,
  width: Dimensions.get("window").width
};

class QuestionPageSubjective extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      isLoading: true,
      routes: [
        { key: "1", title: "1", page: null },
        { key: "2", title: "2", page: null },
        { key: "3", title: "3", page: null },
        { key: "4", title: "4", page: null },
        { key: "5", title: "5", page: null }
      ],
      lineColor: ""
    };
  }
  inputAnswer(key, data, value) {
    // console.log(key+" -- "+value);
    data.answered_status = 1;
    data.answer[0].answer_text = value;

    this.setState({ ...this.state });
    setStore("question_session", this.state.routes);
    // console.log(this.state.routes);
  }

  calculateDataSubj = () => {
    let answered = 0;
    let notAnswered = 0;

    let sessionId = 0;
    let userId = 0;

    let topicId = 0;
    let subtopicId = 0;

    getStore("session_subjective").then(value => {
      console.log(value);
      value = JSON.parse(value);
      sessionId = value.sess_id;
      topicId = value.topic_id;
      subtopicId = value.subtopic_id;
      console.log(sessionId+"-"+topicId+"-"+subtopicId);
    });

    getStore("profile_user_id").then(value => {
      userId = value;
    });

    getStore("question_session").then(value => {
      let dataQuestion = JSON.parse(value);
      console.log(dataQuestion);
      let questionArr = [];
      let ansInput = "";

      for (var i = 0; i < dataQuestion.length; i++) {
        // console.log(dataQuestion[i].key);
        ansInput = "";
        if (dataQuestion[i].questionData.answered_status == 0) {
          notAnswered++;
          ansInput = "";
        } else {
          answered++;
          ansInput = "";
          ansInput = dataQuestion[i].questionData.answer[0].answer_text;
          //   for(var j=0;j<dataQuestion[i].questionData.answer.length;j++){
          //     if(dataQuestion[i].questionData.answer[j].answered_status == 1 && dataQuestion[i].questionData.answer[j].answer_status == true){
          //       correct++;
          //     }else if(dataQuestion[i].questionData.answer[j].answered_status == 1 && dataQuestion[i].questionData.answer[j].answer_status == false){
          //       wrong++;
          //     }
          //   }
        }
        console.log("resultquestionData" + ansInput);
        questionArr.push({
          "answer_input":ansInput,
          "parent_id":dataQuestion[i].questionData.question_id_parent,"question_id":dataQuestion[i].questionData.question_id,
          "time_taken":0,"topic_id":topicId,"subtopic_id":subtopicId})
      }
      let results = {"result":questionArr}

      console.log(`http://api.hometutor.com.my/mobile_api/index.php?process=subjective_end_session&user_id=${userId}&session_id=${sessionId}&question=${JSON.stringify(results)}`);

      fetch(`http://api.hometutor.com.my/mobile_api/index.php?process=subjective_end_session&user_id=${userId}&session_id=${sessionId}&question=${JSON.stringify(results)}`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({})
      })
        .then(res => res.json())
        .then(responseJson => {
          let statusUpdate = responseJson.items.status;
          console.log(responseJson);
          if(statusUpdate == 1){
            console.log('success');
          }
        })
        .catch(error => console.log(error));


      // console.log("resultquestion"+answered+"-"+notAnswered);
      let dataTemp = {
        item: { answered: answered, not_answered: notAnswered }
      };
      // console.log(dataTemp);
      setStore("summary_data_subj", dataTemp);
      // console.log('still in qps');
      Actions.summarySubjective();
    });
  };

  RoutePage(key, obj) {
    if (obj == undefined) {
      obj = {};
      obj.name = "Fakhri";
    }
    // console.log(obj.name);

    // console.log("Nak tahu obj")
    // console.log(key)
    // console.log(obj)
    // console.log(this.state)

    const questionText = obj.question_text;
    const questionTextParent = obj.question_text_parent;

    return (
      <View style={{ width: "100%", alignItems: "center", height: "97%" }}>
        <View style={styles.cardContainer}>
          <View style={{ flexDirection: "row", width: "100%", padding: 10 }}>
            <View style={{ width: "80%" }}>
              <Text>
                {obj.question_code_parent == null ||
                obj.question_code_parent == ""
                  ? obj.question_code
                  : obj.question_code_parent + " - " + obj.question_code}
              </Text>
            </View>
            <View style={{ width: "10%" }}>
              <MaterialCommunityIcons name="information-outline" size={20} />
            </View>
            <View style={{ width: "10%" }}>
              <MaterialCommunityIcons name="flag" size={20} />
            </View>
          </View>
          <ScrollView style={{ width: "100%" }}>
            <View
              style={{ paddingLeft: 10, paddingRight: 10, paddngBottom: 20 }}
            >
              <View style={{ paddingBottom: 20 }}>
                <Text>Pilih jawapan yang paling tepat</Text>
              </View>
              <View>
                <View
                  style={{
                    alignItems: "center",
                    marginLeft: 10,
                    marginRight: 10
                  }}
                ><ScrollView horizontal={true}>
                  {obj.question_image_parent.path != null && obj.question_image_parent.path.trim() != "" ? 
                    <Image
                      source={{ uri: obj.question_image_parent.path }}
                      width={obj.question_image_parent.width}
                      // style={{ maxWidth:'100%',width:obj.question_image_parent.width}}
                    />
                   : null}
                  </ScrollView>
                </View>

                <HTML html={questionTextParent} />
                <View style={{ paddingTop: 15 }}>
                  <Text>Choose your answer for the question</Text>
                </View>

                <View>
                  <ScrollView horizontal={true}>
                  {obj.question_image.path != null && obj.question_image.path != "" ? (
                    <Image
                      source={{ uri: obj.question_image.path }}
                      // width={Dimensions.get("window").width}
                      // style={{ maxWidth:'100%',width:obj.question_image.width}}
                      width={obj.question_image_parent.width}
                    />
                  ) : null}
                  </ScrollView>
                </View>

                <View style={{ paddingLeft: 10, paddingRight: 10 }}>
                  {/* <Text>{obj.question_text}</Text> */}
                  {/* imagesMaxWidth={Dimensions.get('window').width}  */}
                  {/* <HTML html={obj.question_text} /> */}
                  {console.log(obj.question_text)}
                  <HTML html={questionText} />
                </View>
              </View>
            </View>
            <View style={{ padding: 10, marginBottom: 10 }}>
              <Text>Answer</Text>
              <View
                style={{
                  borderWidth: 1,
                  borderColor: "silver",
                  elevation: 10,
                  marginTop: 10,
                  borderRadius: 4
                }}
              >
                <View
                  style={{
                    paddingLeft: 10,
                    paddingRight: 10,
                    paddingBottom: 10,
                    paddingTop: 20
                  }}
                >
                  <TextInput
                    placeholder="Test"
                    selectionColor="pink"
                    onChangeText={value => this.inputAnswer(key, obj, value)}
                  />

                  <View style={{ borderBottomWidth: 1, borderColor: "grey" }} />
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }

  static title = "Scrollable top bar";
  static backgroundColor = "#3f51b5";
  static appbarElevation = 0;

  // state = {
  //   index: 0,
  //   routes: [
  //     { key: "progress", title: "Progress" },
  //     { key: "performance", title: "Performance" }
  //   ]
  // };

  componentDidMount() {
    // console.log(`http://api.hometutor.com.my/mobile_api/index.php?process=essay&level_id=${this.props.level_id}&subject_id=${this.props.subject_id}&topic_id=${this.props.topic_id}&is_active=1`);
    fetch(
      `http://api.hometutor.com.my/mobile_api/index.php?process=essay&level_id=${
        this.props.level_id
      }&subject_id=${this.props.subject_id}&topic_id=${
        this.props.subtopic_id
      }&is_active=1`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({})
      }
    )
      .then(response => response.json())
      .then(responseJson => {
        var itemData = responseJson.items;
        this.state.routes.map(item => {
          let obj = {};
          obj.name = item.title;
          const pages = () => this.RoutePage(item.key, obj);
          item.page = pages;
          item.questionData = itemData.essay[obj.name - 1];
          obj = item.questionData;
        });
        this.setState({
          isLoading: false,
          ...this.state
        });
      })
      .then(data => {
        // console.log("routessss"+this.state.routes);
        setStore("question_session", this.state.routes);
      })
      .catch(error => {
        console.error(error);
      });

    // this.state.routes.map(item => {
    //   let obj = {};
    //   obj.name = item.title;
    //   const pages = () => RoutePage(obj);
    //   item.page = pages;
    // });
    //   this.setState({
    //     isLoading: false,
    //     ...this.state
    //   });
  }

  // calculateData = () => {
  //   let quesAnswered = 0;
  //   let quesNotAnswered = 0;

  //   getStore("question_session").then(value => {
  //     let dataQuestion = JSON.parse(value);
  //     // console.log(dataQuestion);

  //     for(var i=0;i<dataQuestion.length;i++){
  //       // console.log(dataQuestion[i].key);
  //       if(dataQuestion[i].questionData.answered_status == 0){
  //         quesNotAnswered++;
  //       }else{
  //         quesAnswered++;
  //       }
  //     }

  //     console.log("resultquestion"+quesNotAnswered+"-"+quesAnswered);
  //     // return({item :{skipped: skipped,correct:correct,wrong:wrong}});
  //     // let dataTemp = {item :{skipped: skipped,correct:correct,wrong:wrong}};
  //     // console.log(dataTemp);
  //     // setStore('summary_data',dataTemp);
  //     // console.log('still in qp');
  //     // Actions.summary();
  //   });
  // };

  _handleIndexChange = index =>
    this.setState({
      index
    });

  _renderTabBar = props => (
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={styles.indicator}
      style={styles.tabbar}
      tabStyle={styles.tab}
      labelStyle={styles.label}
    />
  );

  // _renderScene = SceneMap({
  //   1: Dashboard_Progress,
  //   2:Dashboard_Performance

  // });

  renderTabTab = () => {
    const obj = Object.assign(
      {},
      ...this.state.routes.map(item => {
        return { [item.key]: item.page };
      })
    );

    // console.log(obj)
    // let obj = {

    //   1:this.state.routes[0].page,
    //   2:this.state.routes[1].page,
    //   3:this.state.routes[2].page,
    //   4:this.state.routes[3].page,
    //   5:this.state.routes[4].page
    //  }
    return (_renderScene = SceneMap(obj));
  };

  render() {
    if (this.state.routes[0].page == null) {
      return (
        <View style={{ flex: 1, paddingTop: 20 }}>
          <ActivityIndicator />
        </View>
      );
    } else {
      return (
        <View style={styles.container2}>
          <View style={{}}>
            <TabView
              style={[styles.container, this.props.style]}
              navigationState={this.state}
              renderScene={this.renderTabTab()}
              renderTabBar={this._renderTabBar}
              onIndexChange={this._handleIndexChange}
              initialLayout={initialLayout}
            />
          </View>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container2: {
    width: "100%",
    alignItems: "center",
    height: "100%"
  },
  cardContainer: {
    width: "95%",
    height: "98%",
    backgroundColor: "white",
    borderRadius: 2,
    elevation: 5,
    marginTop: 10,
    marginBottom: 10
  },
  scene: {
    flex: 1
  },
  container: {
    flex: 1
  },
  tabbar: {
    backgroundColor: "#fff"
  },
  tab: {
    width: 50
  },
  indicator: {
    backgroundColor: "#6047ab"
  },
  label: {
    color: "black",
    fontWeight: "bold"
  }
});

export default QuestionPageSubjective;

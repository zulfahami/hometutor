import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  Dimensions
} from "react-native";
import {
  TabView,
  TabBar,
  SceneMap,
  Route,
  NavigationState
} from "react-native-tab-view";

import PracticeReport from "../components/PracticeReport2";
import SubjectiveReport from "../components/SubjectiveReport2";
import {setStore,getStore} from "../live_api/Utilities";

const initialLayout = {
  height: 0,
  width: Dimensions.get("window").width
};

class Report extends Component {
//   static title = "Scrollable top bar";
  static backgroundColor = "#3f51b5";
  static appbarElevation = 0;

  state = {
    index: 0,
    routes: [
      { key: "practice", title: "Practice" },
      { key: "subjective", title: "Subjective" }
    ]
  };

  componentDidMount() {

    // getStore("all_login_data").then(value => { 
    //   let dataUser = JSON.parse(value); 
    //   let userId = dataUser.profile.user_id;
    //   let packageId = dataUser.packages.id;
    //   console.log(`http://api.hometutor.com.my/mobile_api/index.php?process=scorecard_new_rn&package_id=${packageId}&user_id=${userId}`);
    //   fetch(
    //     `http://api.hometutor.com.my/mobile_api/index.php?process=scorecard_new_rn&package_id=${packageId}&user_id=${userId}`,
    //     {
    //       method: "POST",
    //       headers: {
    //         Accept: "application/json",
    //         "Content-Type": "application/json"
    //       },
    //       body: JSON.stringify({})
    //     }
    //   )
    //     .then(response => response.json())
    //     .then(responseJson => {
    //       var itemData = responseJson.items;
    //       itemData = JSON.stringify(itemData.scorecard[1]);

    //       console.log('reportData');
    //       console.log(itemData);
  
    //     })
    //     .catch(error => {
    //       console.error(error);
    //     });

    // }).catch(error => {
    //   console.error(error);
    // });
  }



  _handleIndexChange = index =>
    this.setState({
      index
    });

  _renderTabBar = props => (
    <TabBar
      {...props}
      indicatorStyle={styles.indicator}
      style={styles.tabbar}
      tabStyle={styles.tab}
      labelStyle={styles.label}
    />
  );

  _renderScene = SceneMap({
    practice: PracticeReport,
    subjective: SubjectiveReport
  });
  render() {
    return (
      <View style={{width:'100%', height:'100%'}}>
        <TabView
          style={[styles.container, this.props.style]}
          navigationState={this.state}
          renderScene={this._renderScene}
          renderTabBar={this._renderTabBar}
          onIndexChange={this._handleIndexChange}
          initialLayout={initialLayout}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
    },
    tabbar: {
        backgroundColor: '#6047ab',
      },
      tab: {
        
      },
      indicator: {
        backgroundColor: 'cyan',
      },
      label: {
        color: '#fff',
        fontWeight: '400',
      },
});

export default Report;

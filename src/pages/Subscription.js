import React, { Component } from "react";
import { View, Text, Alert, ScrollView, StyleSheet, Image } from "react-native";
import { getStore } from "../live_api/Utilities";
import { getDataSubscription } from "../live_api/Subscription";

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'

class Subscription extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user_id: "",
      sb_fullname: "",
      sb_screenname: "",
      sb_activationcode: "",
      sb_activateddate: "",
      sb_expireddate: "",
      sb_packagename: "",
      sb_levelname: "",
      sb_username: "",
      sb_email: "",
      sb_gender: ""
    };
  }

  componentDidMount() {
    getStore("profile_user_id")
      .then(value => {
        this.setState({ user_id: value }); //get id for api
        // Alert.alert(value);
      })
      .then(value => {
        getDataSubscription(this.state.user_id); //get data from api
      })
      .then(value => {
        getStore("profile_fullname").then(value => {
          this.setState({ sb_fullname: JSON.parse(value) });
        });
      })
      .then(value => {
        getStore("profile_display_name").then(value => {
          this.setState({ sb_screenname: JSON.parse(value) });
        });
      })
      .then(value => {
        getStore("activationcode").then(value => {
          this.setState({ sb_activationcode: JSON.parse(value) });
        });
      })
      .then(value => {
        getStore("activated_date").then(value => {
          this.setState({ sb_activateddate: JSON.parse(value) });
        });
      })
      .then(value => {
        getStore("expired_date").then(value => {
          this.setState({ sb_expireddate: JSON.parse(value) });
        });
      })
      .then(value => {
        getStore("package_name").then(value => {
          this.setState({ sb_packagename: JSON.parse(value) });
        });
      })
      .then(value => {
        getStore("profile_school_level").then(value => {
          this.setState({ sb_levelname: JSON.parse(value) });
        });
      })
      .then(value => {
        getStore("profile_username").then(value => {
          this.setState({ sb_username: JSON.parse(value) });
        });
      })
      .then(value => {
        getStore("profile_email").then(value => {
          this.setState({ sb_email: JSON.parse(value) });
        });
      })
      .then(value => {
        getStore("profile_gender").then(value => {
          this.setState({ sb_gender: JSON.parse(value) });
        });
      });
  }

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.backgroundImage}>
            <Image
              source={require("../images/bg_blue.jpg")}
              style={{ width: "100%" }}
            />
            <View
              style={{
                position: "absolute",
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <View style={{}}>
                <View style={styles.imageProfileCircle}>
                  <Image
                    source={require("../images/trolly.jpg")}
                    style={{ width: 150, height: 150 }}
                  />
                </View>
                <View style={{ marginTop: 10 }}>
                  <Text style={{ textAlign: "center", color: "white", fontSize: 28 }}>
                    Subscription
                  </Text>
                </View>
              </View>
            </View>
          </View>

          <View style={{width:'95%', backgroundColor:'white', elevation:5, marginTop:10, justifyContent:'center'}}>
			<View style={{padding:15, borderBottomWidth:0.5, borderColor:'silver', backgroundColor:'#fcfcfc'}}>
				<Text>
					Account Information
				</Text>
			</View>
             
			<View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='account-outline' size={26}/>
				</View>
				<View style={{justifyContent:'center',width:'87%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.sb_fullname}</Text>
					<Text>Full name</Text>
				</View>
			</View>

			<View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='cast' size={26}/>
				</View>
				<View style={{justifyContent:'center',width:'87%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.sb_screenname}</Text>
					<Text>Screen name</Text>
				</View>
			</View>

            <View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='marker-check' size={26}/>
				</View>
				<View style={{justifyContent:'center',width:'87%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.sb_activationcode}</Text>
					<Text>Activation code</Text>
				</View>
			</View>

            <View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialIcons name='alarm-on' size={26}/>
				</View>
				<View style={{justifyContent:'center',width:'87%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.sb_activateddate}</Text>
					<Text>Activated date</Text>
				</View>
			</View>

            <View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='alarm-off' size={26}/>
				</View>
				<View style={{justifyContent:'center',width:'87%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.sb_expireddate}</Text>
					<Text>Expired date</Text>
				</View>
			</View>

            <View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='cart' size={26}/>
				</View>
				<View style={{justifyContent:'center',width:'87%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.sb_packagename}</Text>
					<Text>Package</Text>
				</View>
			</View>

            <View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='clipboard-account' size={26}/>
				</View>
				<View style={{justifyContent:'center',width:'87%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.sb_levelname}</Text>
					<Text>School level</Text>
				</View>
			</View>

            <View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='verified' size={26}/>
				</View>
				<View style={{justifyContent:'center',width:'87%'}}>
					<Text>Active</Text>
					<Text>Status</Text>
				</View>
			</View>
		</View>
        
        <View style={{width:'95%', backgroundColor:'white', elevation:5, marginTop:10,marginBottom:10, justifyContent:'center'}}>
			<View style={{padding:15, borderBottomWidth:0.5, borderColor:'silver', backgroundColor:'#fcfcfc'}}>
				<Text>
					Account Information
				</Text>
			</View>
             
			<View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='account' size={26}/>
				</View>
				<View style={{justifyContent:'center',width:'87%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.sb_username}</Text>
					<Text>Username</Text>
				</View>
			</View>

			<View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='email' size={26}/>
				</View>
				<View style={{justifyContent:'center',width:'87%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.sb_email ? this.state.sb_email : "N/A"}</Text>
					<Text>Email</Text>
				</View>
			</View>

            <View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='account-multiple' size={26}/>
				</View>
				<View style={{justifyContent:'center',width:'87%'}}>
					<Text>{this.state.sb_gender}</Text>
					<Text>Gender</Text>
				</View>
			</View>
		</View>

        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    alignItems: "center"
  },
  backgroundImage: {
    position: "relative",
    top: 0,
    left: 0,
    justifyContent: "center",
    resizeMode: "cover",
    width: "100%",
    height: 250,
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden"
  },
  imageProfileCircle: {
    height: 150,
    width: 150,
    overflow: "hidden",
    backgroundColor: "silver",
    borderRadius: 150 / 2,
    borderWidth: 1,
    borderColor: "purple"
  }
});

export default Subscription;


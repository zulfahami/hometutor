import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  Dimensions
} from "react-native";

import {
  TabView,
  TabBar,
  SceneMap,
  Route,
  NavigationState
} from "react-native-tab-view";

import Dashboard_Progress from "./Dashboard_Progress";
import Dashboard_Performance from "./Dashboard_Performance";

type State = NavigationState<
  Route<{
    key: string,
    title: string
  }>
>;

const initialLayout = {
  height: 0,
  width: Dimensions.get("window").width
};

class Dashboard extends Component {
  static title = "Scrollable top bar";
  static backgroundColor = "#3f51b5";
  static appbarElevation = 0;

  state = {
    index: 0,
    routes: [
      { key: "progress", title: "Progress" }
      // ,
      // { key: "performance", title: "Performance" }
    ]
  };

  _handleIndexChange = index =>
    this.setState({
      index
    });

  _renderTabBar = props => (
    <TabBar
      {...props}
      
      indicatorStyle={styles.indicator}
      style={styles.tabbar}
      tabStyle={styles.tab}
      labelStyle={styles.label}
    />
  );

  _renderScene = SceneMap({
    progress: Dashboard_Progress,
    performance: Dashboard_Performance,
  
  });

  render() {
    return (
      <ScrollView>
        <View style={styles.backgroundImage}>
          <Image
            source={require("../images/bg_purple.jpg")}
            style={{ width: "100%" }}
          />
          <View
            style={{
              position: "absolute",
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <View style={styles.iconDasboardCircle}>
              <Image
                source={require("../images/header.png")}
                style={{ width: 120, height: 120 }}
              />
            </View>

            <View style={{ marginTop: 10 }}>
              <Text style={{ color: "white", fontSize: 28 }}>Dashboard</Text>
            </View>
          </View>
        </View>
        <View style={{}}>
        <TabView
        style={[styles.container, this.props.style]}
        navigationState={this.state}
        renderScene={this._renderScene}
        renderTabBar={this._renderTabBar}
        onIndexChange={this._handleIndexChange}
        initialLayout={initialLayout}
      />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    position: "relative",
    top: 0,
    left: 0,
    justifyContent: "center",
    resizeMode: "cover",
    width: "100%",
    height: 200,
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden"
  },

  iconDasboardCircle: {
    height: 120,
    width: 120,
    overflow: "hidden",
    backgroundColor: "white",
    borderRadius: 120 / 2,
    borderWidth: 1,
    borderColor: "purple"
  },
  container: {
    flex: 1,
  },
  tabbar: {
    backgroundColor: '#3f51b5',
  },
  tab: {
    
  },
  indicator: {
    backgroundColor: '#ffeb3b',
  },
  label: {
    color: '#fff',
    fontWeight: '400',
  },
});

export default Dashboard;

import React, { Component } from "react";
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  TouchableOpacity,
  AsyncStorage
} from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { Actions } from "react-native-router-flux";
const getStore = async key => {
  var findData = "";
  try {
    findData = (await AsyncStorage.getItem(key)) || "none";
    return findData;
  } catch (error) {
    // Error retrieving data
    console.log(error.message);
  }
};
export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = { sFullname: "" };
  }

  componentDidMount() {
    getStore("profile_fullname").then(value => {
      this.setState({ sFullname: JSON.parse(value) });
    });
  }

  practice() {
    Actions.practice();
  }

  subjective() {
    Actions.subjective();
  }

  countUpTimer(){
    Actions.countUpTimer();
  }

  questionPage(){
    Actions.questionPage();
  }

  dashboard(){
    Actions.dashboard();
  }

  render() {
    return (
      <SafeAreaView>
        <View style={styles.container}>
          <View style={styles.backgroundImage}>
            <Image
              source={require("../images/bg_royal_blue.jpg")}
              style={{ width: "100%" }}
            />

            <View
              style={{
                position: "absolute",
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <View style={{}}>
                <View style={styles.imageProfileCircle}>
                  <Image
                    source={require("../images/default_user.png")}
                    style={{ width: 150, height: 150 }}
                  />
                </View>
                <View style={{ marginTop: 10 }}>
                  <Text style={{ textAlign: "center", color: "white" }}>
                    {this.state.sFullname}
                  </Text>
                </View>
              </View>
            </View>
            <View style={{ position: "absolute", right: 0, bottom:0, padding :40 }}>
              <View style={styles.dashboardIconCircle}>
              <TouchableOpacity onPress={this.dashboard}>
                <MaterialCommunityIcons name='trending-up' size={32} color='white'/></TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={{ width: "100%", alignItems: "center" }}>
            <View
              style={{
                width: "96%",
                backgroundColor: "#fff",
                elevation: 5,
                borderRadius: 2,
                marginTop: 10
              }}
            >
              <View style={{ flexDirection: "row" }}>
                <View
                  style={{
                    width: "50%",
                    alignItems: "center",
                    padding: 10,
                    borderRightWidth: 0.4,
                    borderColor: "silver"
                  }}
                >
                  <TouchableOpacity onPress={this.practice}>
                    <Image
                      source={require("../images/icon_exercise.png")}
                      style={{ width: 70, height: 70 }}
                    />
                    <Text>PRACTICE</Text>
                  </TouchableOpacity>
                </View>

                <View
                  style={{ width: "50%", alignItems: "center", padding: 10 }}
                >
                  <TouchableOpacity onPress={this.subjective}>
                    <Image
                      source={require("../images/icon_subjective.png")}
                      style={{ width: 70, height: 70 }}
                    />
                    <Text>SUBJECTIVE</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            <View style={{ alignItems: "center", marginTop: 20 }}>
            <TouchableOpacity onPress={this.countUpTimer}>
              <MaterialCommunityIcons name="clipboard-check" size={50} /></TouchableOpacity>
              <Text
                style={{
                  paddingLeft: 25,
                  paddingRight: 25,
                  textAlign: "center"
                }}
              >
                Take one of activities to improve your performance
              </Text>
            </View>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // backgroundColor:'#f2f2f2'
    height: "100%"
  },
  backgroundImage: {
    position: "relative",
    top: 0,
    left: 0,
    justifyContent: "center",
    resizeMode: "cover",
    width: "100%",
    height: "30%",
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden"
  },

  imageProfileCircle: {
    height: 150,
    width: 150,
    overflow: "hidden",
    backgroundColor: "silver",
    borderRadius: 150 / 2,
    borderWidth: 1,
    borderColor: "purple"
  },

  dashboardIconCircle:{
    width:70,
    height:70,
    borderRadius:70/2,
    backgroundColor:'#7755c7',
    elevation:2,
    justifyContent:'center',
    alignItems:'center'
  }
});

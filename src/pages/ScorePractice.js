import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Image,
  SafeAreaView,
  Dimensions,
  ActivityIndicator
} from "react-native";
import {
  TabView,
  TabBar,
  SceneMap,
  Route,
  NavigationState
} from "react-native-tab-view";

import Score_Correct from "../components/PracticeScoreCorrect";
import Score_Wrong from "../components/PracticeScoreWrong";
import Score_Skipped from "../components/PracticeScoreSkipped";

import { setStore, getStore } from "../live_api/Utilities";

const initialLayout = {
  height: 0,
  width: Dimensions.get("window").width
};

class ScorePractice extends Component {
  state = {
    index: 0,
    routes: [
      { key: "correct", title: "Correct", page: null, scoreCount: 0 },
      { key: "wrong", title: "Wrong", page: null, scoreCount: 0 },
      { key: "skipped", title: "Skipped", page: null, scoreCount: 0 }
    ],
    correct_data: [],
    wrong_data: [],
    skipped_data: []
  };

  _handleIndexChange = index =>
    this.setState({
      index
    });

  _renderTabBar = props => (
    <TabBar
      {...props}
      indicatorStyle={styles.indicator}
      style={styles.tabbar}
      tabStyle={styles.tab}
      labelStyle={styles.label}
    />
  );

  componentDidMount() {
    console.log("did mount score practice");

    getStore("question_session").then(value => {
      let dataQuestion = JSON.parse(value);
      console.log("qd" + dataQuestion);
      let total_skip = [];
      let total_correct = [];
      let total_wrong = [];

      for (var i = 0; i < dataQuestion.length; i++) {
        // console.log(dataQuestion[i].key);
        if (dataQuestion[i].questionData.answered_status == 0) {
          // var skippedTemp = this.state.skipped_data.concat('new value');
          // this.setState({ skipped_data: skippedTemp });

          total_skip = total_skip.concat(dataQuestion[i].questionData);
          console.log("SKIP" + dataQuestion[i].questionData);
          // skipped++;
        } else {
          for (var j = 0; j < dataQuestion[i].questionData.answer.length; j++) {
            if (
              dataQuestion[i].questionData.answer[j].answered_status == 1 &&
              dataQuestion[i].questionData.answer[j].answer_status == true
            ) {
              // var correctTemp = this.state.correct_data.concat('new value');
              // this.setState({ correct_data: correctTemp });

              total_correct = total_correct.concat(dataQuestion[i].questionData);
              console.log("CORRECT" + dataQuestion[i].questionData);
              // correct++;
            } else if (
              dataQuestion[i].questionData.answer[j].answered_status == 1 &&
              dataQuestion[i].questionData.answer[j].answer_status == false
            ) {
              // var wrongTemp = this.state.wrong_data.concat('new value');
              // this.setState({ wrong_data: wrongTemp });

              total_wrong = total_wrong.concat(dataQuestion[i].questionData);
              console.log("WRONG" + dataQuestion[i].questionData);
              // wrong++;
            }
          }
        }
      }

      // console.log("resultquestion"+skipped+"-"+correct+"-"+wrong);
      // return({item :{skipped: skipped,correct:correct,wrong:wrong}});
      // let dataTemp = {item :{skipped: skipped,correct:correct,wrong:wrong}};
      console.log("skipped_data");
      console.log(total_skip);
      console.log("correct_data");
      console.log(total_correct);
      console.log("wrong_data");
      console.log(total_wrong);
      // setStore('summary_data',dataTemp);
      // console.log('still in qp');
      // Actions.summary();
      const scoreSkipped = new Score_Skipped();
      // console.log(scoreSkipped.getScoreSkipped(this.state.skipped_data))
      const scoreCorrect = new Score_Correct();
      // console.log(scoreCorrect.getScoreCorrect(this.state.correct_data))
      const scoreWrong = new Score_Wrong();
      // console.log(scoreWrong.getScoreWrong(this.state.wrong_data))

      this.state.routes[0].page = () => scoreCorrect.getScoreCorrect(total_correct);
      this.state.routes[1].page = () => scoreWrong.getScoreWrong(total_wrong);
      this.state.routes[2].page = () => scoreSkipped.getScoreSkipped(total_skip)

      // this.state.routes[0].title =  this.state.routes[0].title + ' ' + this.circleContainer(total_correct.length);
      console.log("nak cepat")
      console.log(total_correct.length)
      this.state.routes[0].title =  this.state.routes[0].title + ' ' + total_correct.length;
      this.state.routes[1].title =  this.state.routes[1].title + ' ' + total_wrong.length;
      this.state.routes[2].title =  this.state.routes[2].title + ' ' + total_skip.length;

      console.log("nak tau count skipped")
      console.log(this.state.routes[2].scoreCount)
      this.setState({
        ...this.state
      });
    });
  }

  circleContainer = (total) => {
    return (<View><Text style={{color:'yellow'}}>{total}</Text></View>)
  }

//   renderTabTab = () => {
//     const obj = Object.assign(
//       {},
//       ...this.state.routes.map(item => {
//         return { [item.key]: item.page };
//       })
//     );

//     return (_renderScene = SceneMap(obj));
//   };
renderTabTab = () => {
	return _renderScene = SceneMap({
		correct: this.state.routes[0].page,
		wrong: this.state.routes[1].page,
		skipped: this.state.routes[2].page
	  });
  };

  

  render() {
    console.log("render");
    if (this.state.routes[0].page == null) {
      return (
        <View style={{ flex: 1, paddingTop: 20 }}>
          <ActivityIndicator />
        </View>
      );
    } else {
      return (
        <SafeAreaView>
          
            <View style={{ alignItem: "center",width: "100%", height: "100%"  }}>
              <View style={styles.backgroundImage}>
                <Image source={require("../images/bg_pink_light.jpg")} />
                <View
                  style={{
                    position: "absolute",
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <View style={styles.iconSummaryCircle}>
                    <Image
                      source={require("../images/pencil_icon.png")}
                      style={{ width: 120, height: 120 }}
                    />
                  </View>
                  <View style={{ marginTop: 10 }}>
                    <Text style={{ color: "white", fontSize: 28 }}>Score</Text>
                  </View>
                </View>
              </View>

              <View style={{width:'100%',height:'70%', paddingBottom:10}}>
                <TabView
                  style={[styles.container, this.props.style]}
                  navigationState={this.state}
                  renderScene={this.renderTabTab()}
                  renderTabBar={this._renderTabBar}
                  onIndexChange={this._handleIndexChange}
                  initialLayout={initialLayout}
                />
              </View>
            </View>
         
        </SafeAreaView>
      );
    }
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    position: "relative",
    top: 0,
    left: 0,
    justifyContent: "center",
    resizeMode: "cover",
    width: "100%",
    height: 200,
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden"
  },

  iconSummaryCircle: {
    height: 120,
    width: 120,
    overflow: "hidden",
    backgroundColor: "white",
    borderRadius: 120 / 2,
    borderWidth: 1,
    borderColor: "purple"
  },
  tabbar: {
    backgroundColor: "#6967fb"
  },
  tab: {},
  indicator: {
    backgroundColor: "cyan"
  },
  label: {
    color: "#fff",
    fontWeight: "bold"
  }
});

export default ScorePractice;

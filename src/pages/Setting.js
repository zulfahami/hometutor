import React, { Component } from "react";
import { View, Text, StyleSheet, Image, ScrollView, Switch } from "react-native";

class Setting extends Component {

    state ={
        switchValue: true,
        switchValue2: false
    };

    _handleToggleSwitch = () => this.setState(state => ({
        switchValue: !state.switchValue
    }));

    _handleToggleSwitch2 = () => this.setState(state => ({
      switchValue2: !state.switchValue2
  }));

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={{width:'100%'}}>
          <View style={styles.backgroundImage}>
            <Image
              source={require("../images/score_bg.jpg")}
              style={{ width: "100%" }}
            />
            <View
              style={{
                position: "absolute",
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <View style={{}}>
                <View style={styles.imageProfileCircle}>
                  <Image
                    source={require("../images/icon_interactive2.png")}
                    style={{ width: 150, height: 150 }}
                  />
                </View>
                <View style={{ marginTop: 10 }}>
                  <Text
                    style={{
                      textAlign: "center",
                      color: "white",
                      fontSize: 28
                    }}
                  >
                    Setting
                  </Text>
                </View>
              </View>
            </View>
          </View>
            <View style={{alignItems:'center', marginTop:10, marginBottom:10}}>
          <View style={styles.settingCard}>
            <View style={{flexDirection:'row', width:'100%'}}>
                <View style={{width:'50%',padding:20,}}>
                    <Text>Tour Guide</Text>
                </View>

                <View style={{width:'50%',padding:20, alignItems:'flex-end'}}>
                    <Switch 
                        onValueChange={this._handleToggleSwitch}
                        value={this.state.switchValue}
                    />
                </View>
            </View>

            <View style={{flexDirection:'row', width:'100%'}}>
                <View style={{width:'50%',padding:20,}}>
                    <Text>Auto Sync</Text>
                </View>

                <View style={{width:'50%',padding:20, alignItems:'flex-end'}}>
                    <Switch 
                        onValueChange={this._handleToggleSwitch2}
                        value={this.state.switchValue2}
                    />
                </View>
            </View>
          </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    alignItems: "center"
  },
  backgroundImage: {
    position: "relative",
    top: 0,
    left: 0,
    justifyContent: "center",
    resizeMode: "contain",
    width: "100%",
    height: 250,
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden"
  },
  imageProfileCircle: {
    height: 150,
    width: 150,
    overflow: "hidden",
    backgroundColor: "purple",
    borderRadius: 150 / 2,
    borderWidth: 1,
    borderColor: "purple"
  },
  settingCard:{
    width:'95%',
    backgroundColor:'white',
    elevation:10,
    
  }
});
export default Setting;

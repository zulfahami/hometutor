import React, {Component} from 'react';
import {View, Text, StyleSheet, Image, ScrollView ,SafeAreaView, TouchableOpacity} from 'react-native';
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import * as Progress from 'react-native-progress';
import { Actions } from "react-native-router-flux";
import {getStore} from "../live_api/Utilities";

class SummarySubjective extends Component {
    constructor(props) {
		super(props);
		this.state = {  
			answered: 0, not_answered: 0, percent: 0
		};
    }
    
    scoreSubjective() {
        Actions.scoreSubjective();
      }

      componentDidMount(){
        //   console.log("did mount summary")
        //   console.log(this.props.item)
          getStore('summary_data_subj').then(value => { 
              let datatemp = JSON.parse(value);
              this.setState({ 
                  answered: datatemp.item.answered,
                  not_answered:datatemp.item.not_answered,
                  percent: (datatemp.item.answered*0.2)
                }); 
            //   console.log("correctValue"+this.state.correct);
            });
        //   console.log(summaryData.item.skipped);

      }


    render(){
        return(
            <SafeAreaView>
                <ScrollView style={{width:'100%', height:'100%'}}>
                    <View style={{alignItem:'center'}}>
                        <View style={styles.backgroundImage}>  
                            <Image source={require("../images/question_instruction_bg.jpg")} />
                            <View
                                style={{
                                position: "absolute",
                                alignItems: "center",
                                justifyContent: "center"
                                }}
                            >
                                <View style={styles.iconSummaryCircle}>
                                    <Image
                                        source={require("../images/pencil_icon.png")}
                                        style={{ width: 120, height: 120 }}
                                    />
                                </View>
                                <View style={{ marginTop: 10 }}>
                                    <Text style={{ color: "white", fontSize: 28 }}>Summary</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{width:'100%', alignItems:'center'}}>
                            {/* summary */}
                            <View style={{width:'95%', backgroundColor:'white', elevation:5, marginTop:10, justifyContent:'center'}}>
                                <View style={{padding:15, borderBottomWidth:0.5, borderColor:'silver', backgroundColor:'#fcfcfc'}}>
                                    <Text>
                                        Manusia
                                    </Text>
                                </View>
                            </View>

                            {/* score */}
                            <TouchableOpacity onPress={this.scoreSubjective}>
                                <View style={{width:'95%', backgroundColor:'white', elevation:5, marginTop:10, justifyContent:'center'}}>
                                    <View style={{padding:15, borderBottomWidth:0.5, borderColor:'silver', backgroundColor:'#fcfcfc'}}>
                                        <Text>
                                            SCORE
                                        </Text>
                                    </View>

                                    <View style={{flexDirection:'row', width:'100%', padding:15}}>
                                        
                                        <View style={{width:'50%',flexDirection:'row'}}>
                                            <View style={{width:'50%'}}>
                                            <View style={styles.iconScoreCircle}>
                                                <View>
                                                    <Text style={{color:'#ffffff',fontSize:15}}>{this.state.answered}</Text>
                                                </View>
                                                <View style={{borderTopWidth:0.5, borderColor:'#ffffff'}}>
                                                    <Text style={{color:'#ffffff'}}>5</Text>
                                                </View>
                                            </View>
                                            </View>
                                            <View style={{justifyContent:'center', padding:15, width:'50%'}}>
                                                <Text>QUESTIONS ANSWERED</Text>
                                            </View>
                                        </View>

                                        <View style={{width:'50%',borderLeftWidth:0.5, borderColor:'#dddddd', justifyContent:'center', padding:15}}>
                                            <Text>Your score is determined by the number of questions answered correctly</Text>
                                        </View>
                                    </View>

                                    <View style={{flexDirection:'row', width:'100%', alignItems:'center', padding:15}}>
                                        <View style={{alignItems:'center',width:'50%'}}>
                                            <MaterialCommunityIcons name="check" size={30} color='green' />
                                            <Text>{this.state.answered} ANSWERED</Text>
                                        </View>
                                        <View style={{alignItems:'center',width:'50%'}}>
                                            <MaterialCommunityIcons name="chevron-double-right" size={30} color='orange' />
                                            <Text>{this.state.not_answered} SKIPPED</Text>
                                        </View>
                                    </View>
                            
                                </View>
                            </TouchableOpacity>

                            {/* stat */}
                            <View style={{width:'95%', backgroundColor:'white', elevation:5, marginTop:10, justifyContent:'center'}}>
                                <View style={{padding:15, borderBottomWidth:0.5, borderColor:'silver', backgroundColor:'#fcfcfc'}}>
                                    <Text>
                                        STATS
                                    </Text>
                                </View>

                                <View style={{flexDirection:'row', width:'100%',}}>
                                    <View style={{alignItems: 'center',width:'100%',padding:15}}>
                                        <Progress.Circle 
                                            animated={true} 
                                            progress={this.state.percent}  
                                            size={100} 
                                            endAngle={0.9}
                                            thickness={3}
                                            Text="100%"
                                            showsText={true}
                                            direction="clockwise"
                                            strokeCap="square"
                                            fill="green" />
                                        <Text style={{padding:15}}>AVG. SPEED / QUES</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={{alignItems:'center', marginTop:15, marginBottom:10}}>
                            <View>
                                <MaterialCommunityIcons name="forum" size={30}/>
                            </View>
                            <View style={{marginTop:10,width:'97%'}}>
                                <Text style={{textAlign:'center',}}>
                                    Subjective test are more challenging and expensive to prepare, administer
                                    and evaluate correctly, but they can be more valid.
                                </Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>

        )
    }
}

const styles = StyleSheet.create({
    backgroundImage: {
      position: "relative",
      top: 0,
      left: 0,
      justifyContent: "center",
      resizeMode: "cover",
      width: "100%",
      height: 200,
      justifyContent: "center",
      alignItems: "center",
      overflow: "hidden"
    },
  
    iconSummaryCircle: {
      height: 120,
      width: 120,
      overflow: "hidden",
      backgroundColor: "white",
      borderRadius: 120 / 2,
      borderWidth: 1,
      borderColor: "purple"
    },

    iconScoreCircle: {
        height: 90,
        width: 90,
        overflow: "hidden",
        backgroundColor: "#6967fb",
        borderRadius: 90 / 2,
        borderWidth: 1,
        borderColor: "#6967fb",
        alignItems:'center',
        justifyContent:'center'
    }
  });


export default SummarySubjective;
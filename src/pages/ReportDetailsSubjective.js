import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

class ReportDetailsSubjective extends Component {
  // state = {};
  constructor(props) {
    super(props);
    this.state = {
    sessionData: {}
    };
  }

  componentDidMount() {
    // console.log(this.props.item.session_id);
    // console.log(this.props.item.user_id);
    // console.log(`http://api.hometutor.com.my/mobile_api/index.php?process=scorecard_report&module=exercise&user_id=${this.props.item.user_id}&session_id=${this.props.item.session_id}`);
    // http://api.hometutor.com.my/mobile_api/index.php?process=scorecard_report&module=exercise&user_id=3631&session_id=42716

    fetch(
      `http://api.hometutor.com.my/mobile_api/index.php?process=scorecard_report_rn&module=subjective&user_id=${this.props.item.user_id}&session_id=${this.props.item.session_id}`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({})
      }
    )
      .then(response => response.json())
      .then(responseJson => {
        var itemData = responseJson.items.scorecard_report;
        this.setState({
          sessionData : itemData
        });
      })
      // .then(data => {
        // console.log(this.state.sessionData);
        // setStore("question_session", this.state.routes);
      // })
      .catch(error => {
        console.error(error);
      });

  }
  render() {
    // console.log(this.state.sessionData);
    return (
      <View>
        <View style={{ flexDirection: "row", padding: 15, paddingLeft:25 , backgroundColor:'#fff'}}>
          <View>
            <MaterialCommunityIcons name="account-box" size={22} color="#6047ab"/>
          </View>
          <View>
            <Text style={{textAlign: 'center',fontSize:20}}> Session {this.props.item.session_text} </Text>
          </View>
        </View>

        <View
          style={{
            borderBottomColor: "black",
            borderBottomWidth: 1
          }}
        />

        <View style={{
            borderColor: "#aaa",
            borderWidth: 1,
            margin:25,
            // marginTop: 3,
            // marginLeft: 10,
            // marginRight:10
            borderRadius:2
          }}>
          <View
          style={{
            borderBottomColor: "#aaa",
            borderBottomWidth: 1,
            padding:10,
            backgroundColor:'#6047ab'
          }}>
            <Text style={{textAlign: 'center',fontSize:20, color:'#fff'}}> {this.state.sessionData.topic_name} </Text>
          </View>
          <View
          style={{
            borderBottomColor: "#aaa",
            borderBottomWidth: 1,
            flexDirection:"row",
            width:'100%',
            padding:10,
            backgroundColor:'#fff'
          }}>
            <View style={{width:'50%'}}><Text style={{textAlign: 'left',fontSize:20}}> Total Mark </Text></View>
            <View style={{width:'50%'}}><Text style={{textAlign: 'right',fontSize:20}}> {this.state.sessionData.total_answered*20}% </Text></View>
          </View>
          <View
          style={{
            borderBottomColor: "#aaa",
            borderBottomWidth: 1,
            flexDirection:"row",
            width:'100%',
            padding:10,
            backgroundColor:'#fff'
          }}>
            <View style={{width:'50%'}}><Text style={{textAlign: 'left',fontSize:20}}> Total Answer Question </Text></View>
            <View style={{width:'50%'}}><Text style={{textAlign: 'right',fontSize:20}}> {this.state.sessionData.total_answered} </Text></View>
          </View>
          <View
          style={{
            borderBottomColor: "#aaa",
            borderBottomWidth: 1,
            flexDirection:"row",
            width:'100%',
            padding:10,
            backgroundColor:'#fff'
          }}>
            <View style={{width:'50%'}}><Text style={{textAlign: 'left',fontSize:20}}> Total Skipped Answer </Text></View>
            <View style={{width:'50%'}}><Text style={{textAlign: 'right',fontSize:20}}> {5 - this.state.sessionData.total_answered} </Text></View>
          </View>
          <View
          style={{
            borderBottomColor: "#aaa",
            borderBottomWidth: 1,
            flexDirection:"row",
            width:'100%',
            padding:10,
            backgroundColor:'#fff'
          }}>
            <View style={{width:'50%'}}><Text style={{textAlign: 'left',fontSize:20}}> Total Time Taken </Text></View>
            <View style={{width:'50%'}}><Text style={{textAlign: 'right',fontSize:20}}> {this.state.sessionData.total_time} </Text></View>
          </View>
          <View
          style={{
            flexDirection:"row",
            width:'100%',
            padding:10,
            backgroundColor:'#fff'
          }}>
            <View style={{width:'50%'}}><Text style={{textAlign: 'left',fontSize:20}}> Average Time Taken </Text></View>
            <View style={{width:'50%'}}><Text style={{textAlign: 'right',fontSize:20}}> {this.state.sessionData.average_time} </Text></View>
          </View>
        </View>
        <View>
          {/* <Text> Dismiss </Text> */}
        </View>
      </View>
    );
  }
}

export default ReportDetailsSubjective;

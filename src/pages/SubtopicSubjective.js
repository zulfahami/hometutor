import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  Image
} from "react-native";
import { Actions } from "react-native-router-flux";

class SubtopicSubjective extends Component {
  constructor(props) {
    super(props);
    state = {};
  }

  questionInstructionSubjective(item) {
    console.log(item);
    Actions.questionInstructionSubjective(item);
  }

  render() {
    const subtopicList = [];
    // console.log(this.props.data.subtopic.length);
    // console.log(this.props.data.topic);
    for (let x = 0; x < this.props.data.subtopic.length; x++) {
      // count++;
      // console.log(x);
      // console.log(this.props.data.subtopic[x]);

      // const item = this.props.subtopic[x];
      subtopicList.push(
        <View
          key={x}
          style={{ flexDirection: "row", width: "100%", marginTop: 10 }}
        >
          <View style={{ width: "15%" }}>
            <Image
              source={require("../images/icon_file_in_box.png")}
              style={{ height: 45, width: 45 }}
            />
          </View>

          <View
            style={{ paddingLeft: 20, width: "65%", justifyContent: "center" }}
          >
            <Text>{this.props.data.subtopic[x].subtopic_name}</Text>
          </View>
          <View
            key={x}
            style={{
              alignItems: "flex-end",
              width: "20%",
              justifyContent: "center"
            }}
          >
            {/* <Text style={{}}>
									{this.props.data.topic.topic_id}
								</Text>
								<Text style={{}}>
									{this.props.data.subtopic[x].subtopic_name}
								</Text>
								<Text style={{}}>
									{this.props.data.subtopic[x].subtopic_id}
								</Text> */}

            <TouchableOpacity
              onPress={() =>
                this.questionInstructionSubjective({
                  item: {
                    topic_id: this.props.data.topic.topic_id,
                    subtopic_id: this.props.data.subtopic[x].subtopic_id,
                    level_id: this.props.data.topic.level_id,
                    subject_id: this.props.data.topic.subject_id,
                    topic_name: this.props.data.topic.topic_name,
                    subtopic_name: this.props.data.subtopic[x].subtopic_name
                  }
                })
              }
            >
              <View
                style={{
                  paddingTop: 6,
                  paddingBottom: 6,
                  borderWidth: 1,
                  borderRadius: 3,
                  borderColor: "green",
                  paddingLeft: 10,
                  paddingRight: 10
                }}
              >
                <Text style={{ color: "green" }}>Start</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <SafeAreaView>
          <ScrollView>
            <View style={{ alignItems: "center" }}>
              <View style={styles.backgroundImage}>
                <Image source={require("../images/bg_yellow.jpg")} />
                <View
                  style={{
                    position: "absolute",
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <View style={styles.iconPracticeCircle}>
                    <Image
                      source={require("../images/header_icon_exercise.jpg")}
                      style={{ width: 120, height: 120 }}
                    />
                  </View>
                  <View style={{ marginTop: 10 }}>
                    <Text style={{ color: "white", fontSize: 28 }}>{}</Text>
                  </View>
                </View>
              </View>
              <View
                style={{
                  backgroundColor: "#fff",
                  marginTop: 10,
                  width: "95%",
                  alignItems: "center",
                  paddingBottom: 10
                }}
              >
                <View
                  style={{
                    borderBottomWidth: 1,
                    borderColor: "green",
                    width: "90%"
                  }}
                >
                  <Text style={{ textAlign: "center", padding: 15 }}>
                    {this.props.data.topic.topic_name}
                  </Text>
                </View>
                <View style={{ width: "90%" }}>{subtopicList}</View>
                {/* <Text>
					Subtopic List Practice
				</Text> */}
              </View>
            </View>
          </ScrollView>
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    alignItems: "center"
  },
  backgroundImage: {
    position: "relative",
    top: 0,
    left: 0,
    justifyContent: "center",
    resizeMode: "cover",
    width: "100%",
    height: 200,
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden"
  },

  iconPracticeCircle: {
    height: 120,
    width: 120,
    overflow: "hidden",
    backgroundColor: "white",
    borderRadius: 120 / 2,
    borderWidth: 1,
    borderColor: "purple"
  }
});

export default SubtopicSubjective;

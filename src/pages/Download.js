import React, { Component } from "react";
import { View, Text, StyleSheet, Image, ScrollView } from "react-native";

class Download extends Component {
  constructor(props) {
    super(props);
    this.state = {
      timerStart: false,
      stopwatchStart: false,
      totalDuration: 90000,
      timerReset: false,
      stopwatchReset: false,
    };
    this.toggleTimer = this.toggleTimer.bind(this);
    this.resetTimer = this.resetTimer.bind(this);
    this.toggleStopwatch = this.toggleStopwatch.bind(this);
    this.resetStopwatch = this.resetStopwatch.bind(this);
  }

  toggleTimer() {
    this.setState({timerStart: !this.state.timerStart, timerReset: false});
  }

  resetTimer() {
    this.setState({timerStart: false, timerReset: true});
  }

  toggleStopwatch() {
    this.setState({stopwatchStart: !this.state.stopwatchStart, stopwatchReset: false});
  }

  resetStopwatch() {
    this.setState({stopwatchStart: false, stopwatchReset: true});
  }
  
  getFormattedTime(time) {
      this.currentTime = time;
  };
  render() {
    return (
      <View style={styles.container}>
      {/* <ScrollView style={{width:'100%'}}>
        <View style={styles.backgroundImage}>
          <Image
            source={require("../images/bg_blue.jpg")}
            style={{ width: "100%" }}
          />
          <View
            style={{
              position: "absolute",
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <View style={{}}>
              <View style={styles.imageProfileCircle}>
                <Image
                  source={require("../images/icon_assessment2.png")}
                  style={{ width: 150, height: 150 }}
                />
              </View>
              <View style={{ marginTop: 10 }}>
                <Text
                  style={{ textAlign: "center", color: "white", fontSize: 28 }}
                >
                  Download
                </Text>
              </View>
            </View>
          </View>
        </View>

            <View style={{alignItems:'center'}}>
        <View style={styles.DownloadCard}>
            <View style={{alignItems:'center', padding: 10}}>
                <Text>
                    Standard 4
                </Text>
            </View>
            <View style={{flexDirection:'row', width:'100%'}}>
                    <View style={{width:'50%'}}>
                        <Text>Topic</Text>
                    </View>
                    <View style={{width:'50%', alignItems:'flex-end'}}>
                        <Text>Icon</Text>
                    </View>
                </View>
                </View>
                </View>
        </ScrollView> */}


      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    alignItems: "center"
  },
  backgroundImage: {
    position: "relative",
    top: 0,
    left: 0,
    justifyContent: "center",
    resizeMode: "cover",
    width: "100%",
    height: 250,
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden"
  },
  imageProfileCircle: {
    height: 150,
    width: 150,
    overflow: "hidden",
    backgroundColor: "silver",
    borderRadius: 150 / 2,
    borderWidth: 1,
    borderColor: "purple"
  },
  DownloadCard:{
      width:'95%',
      backgroundColor:'white',
      elevation:10,
      borderRadius:2,
      alignItems:'center'
  }
});

export default Download;

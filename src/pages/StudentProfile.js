import React, { Component } from "react";
import { View, Text, StyleSheet, Image, ScrollView,AsyncStorage, Alert } from "react-native";

import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

const getStudentProfile = async key => {
	var findData = "";
	try {
	  findData = (await AsyncStorage.getItem(key)) || "none";
	  return findData;
	} catch (error) {
	  // Error retrieving data
	  console.log(error.message);
	}
  };
  
class StudentProfile extends Component {

	constructor(props) {
		super(props);
		this.state = {  
			user_id: "",
			sp_fullname: "",
			sp_screenname: "",
			sp_package_name: "",
			sp_school_name: "",
			sp_school_level: "",
			sp_username: "",
			sp_email: "",
			sp_gender: ""
		};
	}

	componentDidMount() {
		getStudentProfile("profile_fullname").then(value => { this.setState({ sp_fullname: JSON.parse(value) });
		}).then(value => { getStudentProfile("profile_display_name").then(value => { this.setState({ sp_screenname: JSON.parse(value) }); })
		}).then(value => { getStudentProfile("package_name").then(value => { this.setState({ sp_package_name: JSON.parse(value) }); })
		}).then(value => { getStudentProfile("profile_school_name").then(value => { this.setState({ sp_school_name: JSON.parse(value) }); })
		}).then(value => { getStudentProfile("profile_school_level").then(value => { this.setState({ sp_school_level: JSON.parse(value) }); })
		}).then(value => { getStudentProfile("profile_username").then(value => { this.setState({ sp_username: JSON.parse(value) }); })
		}).then(value => { getStudentProfile("profile_email").then(value => { this.setState({ sp_email: JSON.parse(value) }); })
		}).then(value => { getStudentProfile("profile_gender").then(value => { this.setState({ sp_gender: JSON.parse(value) }); })
		});

	}
	  
  render() {
	return (
		<ScrollView>
	  <View style={styles.container}>
		<View style={styles.backgroundImage}>
		  <Image
			source={require("../images/bg_blue.jpg")}
			style={{ width: "100%", }}
		  />
		  <View
			style={{
			  position: "absolute",
			  alignItems: "center",
			  justifyContent: "center"
			}}
		  >
			<View style={{}}>
			  <View style={styles.imageProfileCircle}>
				<Image
				  source={require("../images/default_user.png")}
				  style={{ width: 150, height: 150 }}
				/>
			  </View>
			  <View style={{ marginTop: 10 }}>
				<Text style={{ textAlign: "center", color: "white" }}>
				  Profile
				</Text>
			  </View>
			</View>
		  </View>
		</View>

		<View style={{width:'95%', backgroundColor:'white', elevation:5, marginTop:10, justifyContent:'center'}}>
			<View style={{padding:15, borderBottomWidth:0.5, borderColor:'silver', backgroundColor:'#fcfcfc'}}>
				<Text>
					Basic Information
				</Text>
			</View>

			<View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='account-outline' size={26}/>
				</View>
				<View style={{justifyContent:'center',width:'60%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.sp_fullname}</Text>
					<Text>Full Name</Text>
				</View>
				<View style={{padding:10,width:'20%',borderBottomWidth:0.5, borderColor:'silver', alignItems:'flex-end', justifyContent:'center'}}>
					{/* <MaterialCommunityIcons name='pencil' size={20} /> */}
				</View>
			</View>

			<View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='cast' size={26}/>
				</View>
				<View style={{paddingTop:10,paddingBottom:10,width:'60%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.sp_screenname}</Text>
					<Text>Screen Name</Text>
				</View>
				<View style={{padding:10,width:'20%',borderBottomWidth:0.5, borderColor:'silver', alignItems:'flex-end'}}>
					{/* <MaterialCommunityIcons name='pencil' size={20} /> */}
				</View>
			</View>

			<View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='cart' size={26}/>
				</View>
				<View style={{paddingTop:10,paddingBottom:10,width:'80%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.sp_package_name}</Text>
					<Text>Pacakage</Text>
				</View>
			</View>

			<View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='school' size={26}/>
				</View>
				<View style={{paddingTop:10,paddingBottom:10,width:'80%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.sp_school_name}</Text>
					<Text>School name</Text>
				</View>
			</View>

			<View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='clipboard-account' size={26}/>
				</View>
				<View style={{paddingTop:10,paddingBottom:10,width:'60%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.sp_school_level}</Text>
					<Text>School Level</Text>
				</View>
				<View style={{padding:10,width:'20%',borderBottomWidth:0.5, borderColor:'silver', alignItems:'flex-end'}}>
					{/* <MaterialCommunityIcons name='pencil' size={20} /> */}
				</View>
			</View>

			<View style={{flexDirection:'row', width:'100%',paddingBottom:10}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='verified' size={26}/>
				</View>
				<View style={{paddingTop:10,paddingBottom:10,width:'60%',}}>
					<Text>Active</Text>
					<Text>Status</Text>
				</View>
			</View>
		</View>

			<View style={{width:'95%', backgroundColor:'white', elevation:5, marginTop:10, justifyContent:'center',marginBottom:10}}>
			<View style={{padding:15, borderBottomWidth:0.5, borderColor:'silver', backgroundColor:'#fcfcfc'}}>
				<Text>
					Personal Information
				</Text>
			</View>

			<View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='account' size={26}/>
				</View>
				<View style={{paddingTop:10,paddingBottom:10,width:'80%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.sp_username}</Text>
					<Text>Username</Text>
				</View>
			</View>

			 <View style={{flexDirection:'row', width:'100%',}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='email' size={26}/>
				</View>
				<View style={{paddingTop:10,paddingBottom:10,width:'80%',borderBottomWidth:0.5, borderColor:'silver'}}>
					<Text>{this.state.sp_email}</Text>
					<Text>Email</Text>
				</View>
			</View>

			 <View style={{flexDirection:'row', width:'100%',paddingBottom:10}}>
				<View style={{margin:20,justifyContent:'center',alignItems: 'center',}}>
					<MaterialCommunityIcons name='human-male-female' size={26}/>
				</View>
				<View style={{paddingTop:10,paddingBottom:10,width:'80%',}}>
					<Text>{this.state.sp_gender}</Text>
					<Text>Gender</Text>
				</View>
			</View>
			</View>
	  </View>
	  </ScrollView>
	);
  }
}

const styles = StyleSheet.create({
	container:{
		width:'100%',
		alignItems: 'center',
	},
  backgroundImage: {
	position: "relative",
	top: 0,
	left: 0,
	justifyContent: "center",
	resizeMode: "cover",
	width: "100%",
	height: 250,
	justifyContent: "center",
	alignItems: "center",
	overflow: "hidden"
  },
  imageProfileCircle: {
	height: 150,
	width: 150,
	overflow: "hidden",
	backgroundColor: "silver",
	borderRadius: 150 / 2,
	borderWidth: 1,
	borderColor: "purple"
  },
});

export default StudentProfile;

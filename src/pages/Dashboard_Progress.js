import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import * as Progress from "react-native-progress";
import { setStore, getStore } from "../live_api/Utilities";

import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

const isOdd =(num)=> { return num % 2;}

class Dashboard_Progress extends Component {
  constructor(props) {
    super(props);
    this.state = {
      progressData: [],
      resultAvg:0
    };
  }
  componentDidMount() {
    getStore("all_login_data")
      .then(value => {
        let dataUser = JSON.parse(value);
        let userId = dataUser.profile.user_id;
        let packageId = dataUser.packages.id;
        console.log(
          `http://api.hometutor.com.my/mobile_api/index.php?process=dashboard_progress&package_id=${packageId}&user_id=${userId}`
        );
        fetch(
          `http://api.hometutor.com.my/mobile_api/index.php?process=dashboard_progress&package_id=${packageId}&user_id=${userId}`,

          {
            method: "POST",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json"
            },
            body: JSON.stringify({})
          }
        )
          .then(response => response.json())
          .then(responseJson => {
            var itemData = responseJson.items.progress;
            // itemData = JSON.stringify(itemData.scorecard[1]);
            var sumResult = 0;

            for (let i = 0; i < itemData.length; i++) {
              sumResult = sumResult + itemData[i].aggregate;
            }

            var average = sumResult / itemData.length;
            console.log(average);
            var resAvg = average / 100;
            // resAvg = Math.round10(resAvg , 0);

            resAvg = resAvg.toFixed(2);
            console.log(resAvg);
            console.log("reportDatapractice");
            console.log(itemData);

            
            this.setState({ progressData: itemData, resultAvg:resAvg });
          })
          .catch(error => {
            console.error(error);
          });
      })
      .catch(error => {
        console.error(error);
      });
  }

  render() {
    console.log("nak tau progress length");
    console.log(this.state.progressData.length);
    const progressBar = [];

    const colorList = ['#7f7e2a', '#3c7f2a', '#207281', '#511e79', '#7f2074'];
    let count = 0;

    for(let x in this.state.progressData){
      count++;
      const item = this.state.progressData[x];
      progressBar.push(
        <View key={x} style={{width:'50%',}}>
        <View style={{width:'100%', borderRightWidth: isOdd(count) === 0 ? 0 : 0.5 ,
                      borderColor: "silver",
                      paddingBottom:20,
                      paddingLeft: isOdd(count) === 0 ? 20 : 0,
                      borderBottomWidth: 0.5 }}>
        <View style={{paddingTop: 4, flex:1, width:'90%' }}>
          <Text style={{paddingTop:10}}>
            {item.name}
          </Text>
          <Text style={{paddingTop:10, paddingBottom:10}}>
           Aggregate {item.aggregate} %
          </Text>
          <Progress.Bar
                  progress={item.aggregate/100}
                  width={null}
                  color={colorList[x] === undefined ? colorList[count -1] : colorList[x] }
                  unfilledColor={"#ddd"}
                  borderColor={"transparent"}
                />
          </View>
          </View>
          </View>
      );

    }

    console.log("dalam render");
    console.log(this.state.progressData);
    return (
      <View style={styles.cardContainer}>
        <View style={{ paddingLeft:20, paddingRight:20, paddingTop:20 }}>
          <View style={{}}>
            <Text>Overall Assessment Score</Text>

            <View
              style={{
                flexDirection: "row",
                paddingTop: 10,
                paddingBottom: 10,
                borderBottomWidth: 0.5,
                borderColor: "silver"
              }}
            >
              {console.log("nak tau value resAvg dalam return")}
              {console.log(this.state.resultAvg)}
              <View style={{ paddingTop: 4, flex: 1, width: "90%" }}>
                <Progress.Bar
                  progress={this.state.resultAvg}
                  width={null}
                  color={"#666666"}
                  unfilledColor={"#ddd"}
                  borderColor={"transparent"}
                />
              </View>
              <View style={{ paddingLeft: 20 }}>
                <Text> {this.state.resultAvg*100} %</Text>
              </View>
            </View>
            <View style={{ flexDirection: "row", width: "100%", flexWrap:'wrap' }}>
            {progressBar}
            </View>
          </View>

          <View style={{margin:13}}>
              {/* <Text>
                  Bahasa Melayu
              </Text> */}

              <View style={{width:'100%',paddingTop:10 }}>
              </View>
          </View>

          <View style={{alignItems:'center',marginBottom:20}}>
            <View>
                  <MaterialCommunityIcons name='poll-box' size={36} />
            </View>
            <View>
              <Text style={{textAlign:'center'}}>This statistics represent your score based on subjects</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container2: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "yellow"
  },
  cardContainer: {
    backgroundColor: "white",
    margin: 13,
    resizeMode: "contain"
  }
});
export default Dashboard_Progress;

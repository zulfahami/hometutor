import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Dimensions,
  ActivityIndicator,
  FlatList,
  WebView
} from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import ReactNativeItemSelect from "react-native-item-select";
import { TabView, TabBar, SceneMap } from "react-native-tab-view";
import {getStore} from '../live_api/Utilities';
import Image from "react-native-scalable-image";
import HTML from "react-native-render-html"

const initialLayout = {
  height: 0,
  width: Dimensions.get("window").width
};

class PracticeReview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      isLoading: true,
      routes: [
        { key: "1", title: "1", page: null, dataQuestion2: null },
        { key: "2", title: "2", page: null, dataQuestion2: null},
        { key: "3", title: "3", page: null, dataQuestion2: null },
        { key: "4", title: "4", page: null, dataQuestion2: null },
        { key: "5", title: "5", page: null, dataQuestion2: null }
      ]
    };
  }

  static title = "Scrollable top bar";
  static backgroundColor = "#3f51b5";
  static appbarElevation = 0;

  RoutePage (obj){
    if (obj == undefined) {
      obj = {};
      obj.name = "Fakhri";
    }
    // console.log(obj.name);
    let itemData = obj.data.questionData;
    let questionTextParent = itemData.question_text_parent;
    return (
      <View style={{ width: "100%", alignItems: "center", height: "97%" }}>
      <View style={styles.cardContainer}>
        <View style={{ flexDirection: "row", width: "100%", padding: 10 }}>
          <View style={{ width: "80%" }}>
            <Text>
            {itemData.question_code_parent == null || itemData.question_code_parent == "" ? itemData.question_code : itemData.question_code_parent +" - "+ itemData.question_code}
            </Text>
          </View>
          <View style={{ width: "10%", paddingLeft: 15 }}>
            <MaterialCommunityIcons name="information-outline" size={20} />
          </View>
          <View style={{ width: "10%", paddingLeft: 10 }}>
            <MaterialCommunityIcons name="flag" size={20} />
          </View>
        </View>
        <ScrollView>
          <View style={{ paddingLeft: 10, paddingRight: 10 }}>
            <View style={{ paddingBottom: 20 }}>
              <Text>Pilih jawapan yang paling tepat</Text>
            </View>
            <View style={{}}>
              <View
                style={{
                  alignItems: "center",
                  marginLeft: 10,
                  marginRight: 10
                }}
              ><ScrollView horizontal={true}> 
                 {itemData.question_image_parent.path != null && itemData.question_image_parent.path.trim() != "" ? 
                    <Image
                      source={{ uri: itemData.question_image_parent.path }}
                      width={itemData.question_image_parent.width}
                    />
                   : null}
                 </ScrollView>
              </View>
              {/* <Text style={{}}>{itemData.question_text_parent}</Text> */}
                <HTML html={questionTextParent} />
              <View style={{ paddingTop: 15 }}>
                <Text>Choose your answer for the question</Text>
              </View>
              <View>
                <ScrollView>
                 {itemData.question_image.path != null && itemData.question_image.path.trim() != "" ? 
                    <Image
                      source={{ uri: itemData.question_image.path }}
                      width={itemData.question_image.width}
                    />
                   : null}
                 </ScrollView>
              </View>
              <View style={{ paddingLeft: 10, paddingRight: 10 }}>
              <HTML html={itemData.question_text}/>
              </View>
            </View>
          </View>
          <View style={{ padding: 10 }}>
            <View style={{ paddingTop: 20 }}>
              <Text>Options</Text>
            </View>
              <FlatList 
                data={itemData.answer}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({item}) => 
                <View>
                {item.answer_status == true ?  <View style={{  width: "100%", padding: 10 , borderWidth:2, borderRadius:4, borderColor:'green', marginTop:10}}>
              <View style={{flexDirection: "row"}}>
              <View
                style={{
                  width: "5%",
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                {
                  item.answer_no == 1 ? <Text>A</Text> : item.answer_no == 2 ? <Text>B</Text> : item.answer_no == 3 ? <Text>C</Text> : <Text>D</Text> 
                }
              </View>
              <View style={{ width: "95%", paddingLeft: 10 }}>
              <ScrollView>
              {item.answer_file.path != null && item.answer_file.path != "" ? <Image
                      source={{ uri: item.answer_file.path }}
                      width={item.answer_file.path.width}
                      // style={{ maxWidth:'100%',width:obj.question_image_parent.width}}
                    /> : <HTML html={item.answer_text}/> }
                </ScrollView>
              </View>
              </View>
              <View style={{padding:10}}>
                <Text style={{color:'green', paddingTop:5}}>Reason</Text>
                <HTML html={item.reason} style={{paddingTop:5}}/>
                {item.reason_file.path != null && item.reason_file.path != "" ? <Image
                      source={{ uri: item.reason_file.path }}
                      width={item.reason_file.path.width}
                      // style={{ maxWidth:'100%',width:obj.question_image_parent.width}}
                    /> : <HTML html={item.reason}/> }
                </View>
            </View> :

            item.answered_status == 0  ?  <View style={{ flexDirection: "row", width: "100%", padding: 10 , borderWidth:2, borderRadius:4, borderColor:'silver', marginTop:10}}>
            <View
              style={{
                width: "5%",
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              {
                item.answer_no == 1 ? <Text>A</Text> : item.answer_no == 2 ? <Text>B</Text> : item.answer_no == 3 ? <Text>C</Text> : <Text>D</Text> 
              }
            </View>
            <View style={{ width: "95%", paddingLeft: 10 }}>
            {item.answer_file.path != null && item.answer_file.path != "" ? <Image
                      source={{ uri: item.answer_file.path }}
                      width={item.answer_file.path.width}
                      // style={{ maxWidth:'100%',width:obj.question_image_parent.width}}
                    /> : <HTML html={item.answer_text}/> }
            </View>
            </View> 

            : 
              
             <View style={{ flexDirection: "row", width: "100%", padding: 10 , borderWidth:2, borderRadius:4, borderColor:'red', marginTop:10}}>
             <View
               style={{
                 width: "5%",
                 alignItems: "center",
                 justifyContent: "center"
               }}
             >
               {
                 item.answer_no == 1 ? <Text>A</Text> : item.answer_no == 2 ? <Text>B</Text> : item.answer_no == 3 ? <Text>C</Text> : <Text>D</Text> 
               }
             </View>
             <View style={{ width: "95%", paddingLeft: 10 }}>
             {item.answer_file != null && item.answer_file != "" ? <Image
                      source={{ uri: item.answer_file.path }}
                      width={item.answer_file.path.width}
                      // style={{ maxWidth:'100%',width:obj.question_image_parent.width}}
                    /> : <HTML html={item.answer_text}/> }
             </View>
           </View> 
            }
               
              </View>
              }
              />
            <View />
          </View>
        </ScrollView>
      </View>
    </View>
    );
  };

  // state = {
  //   index: 0,
  //   routes: [
  //     { key: "progress", title: "Progress" },
  //     { key: "performance", title: "Performance" }
  //   ]
  // };

  componentDidMount() {
    console.log(this.props.obj)
    // this.state.routes.map(item => {
    //   let obj = {};
    //   obj.name = item.title;
    //   const pages = () => this.RoutePage(obj);
    //   item.page = pages;
    // });
    // this.setState({
    //   isLoading: false,
    //   ...this.state
    // });

    getStore("question_session").then(value => { 
      let dataQuestion = JSON.parse(value);  
      console.log(dataQuestion);

      // this.setState({
      //   
      // });
      console.log("nak tengok state routes")
      console.log(this.state.routes);
      count = -1;
      this.state.routes.map(item => {
        count++;
        let obj = {};
        obj.name = item.title;
        const pages = () => this.RoutePage(obj);
        item.page = pages;
        item.dataQuestion2 = dataQuestion[count];
        obj.data = item.dataQuestion2;

        this.setState({
          route: dataQuestion,
          isLoading: false,
          ...this.state
        });
      });
      
    }); //get id for api
    
  }

  _handleIndexChange = index =>
    this.setState({
      index
    });

  _renderTabBar = props => (
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={styles.indicator}
      style={styles.tabbar}
      tabStyle={styles.tab}
      labelStyle={styles.label}
    />
  );

  // _renderScene = SceneMap({
  //   1: Dashboard_Progress,
  //   2:Dashboard_Performance

  // });

  renderTabTab = () => {
    const obj = Object.assign(
      {},
      ...this.state.routes.map(item => {
        return { [item.key]: item.page };
      })
    );

    // let obj = {

    //   1:this.state.routes[0].page,
    //   2:this.state.routes[1].page,
    //   3:this.state.routes[2].page,
    //   4:this.state.routes[3].page,
    //   5:this.state.routes[4].page
    //  }
    return (_renderScene = SceneMap(obj));
  };

  render() {
    if (this.state.routes[0].page == null) {
      return (
        <View style={{ flex: 1, paddingTop: 20 }}>
          <ActivityIndicator />
        </View>
      );
    } else {
      return (
        <View style={styles.container2}>
          <View style={{}}>
            <TabView
              style={[styles.container, this.props.style]}
              navigationState={this.state}
              renderScene={this.renderTabTab()}
              renderTabBar={this._renderTabBar}
              onIndexChange={this._handleIndexChange}
              initialLayout={initialLayout}
            />
          </View>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container2: {
    width: "100%",
    alignItems: "center",
    height: "100%"
  },
  cardContainer: {
    width: "95%",
    height: "99%",
    backgroundColor: "white",
    borderRadius: 2,
    elevation: 5,
    marginTop: 10,
    marginBottom: 10
  },
  scene: {
    flex: 1
  },
  container: {
    flex: 1
  },
  tabbar: {
    backgroundColor: "#fff"
  },
  tab: {
    width: 50
  },
  indicator: {
    backgroundColor: "#6047ab"
  },
  label: {
    color: "black",
    fontWeight: "bold"
  }
});

export default PracticeReview;

import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Dimensions,
  ActivityIndicator,
  FlatList,
  TouchableOpacity,
  WebView,
  
} from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { TabView, TabBar, SceneMap } from "react-native-tab-view";
import Image from "react-native-scalable-image";
import { setStore, getStore } from "../live_api/Utilities";
import { Actions } from "react-native-router-flux";
import HTML from "react-native-render-html";


console.disableYellowBox = true;

const initialLayout = {
  height: 0,
  width: Dimensions.get("window").width
};

class QuestionPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      isLoading: true,
      routes: [
        { key: "1", title: "1", page: null, questionData: null },
        { key: "2", title: "2", page: null, questionData: null },
        { key: "3", title: "3", page: null, questionData: null },
        { key: "4", title: "4", page: null, questionData: null },
        { key: "5", title: "5", page: null, questionData: null }
      ],
      answerData: []
    };
  }

  calculateData = () => {
    // console.log("nak check obj dalam calculateData")
    // console.log(obj)

    // return({item : {correct:10}})

    let correct = 0;
    let wrong = 0;
    let skipped = 0;
    // let dataQuestion = null;

    let sessionId = 0;
    let userId = 0;

    let topicId = 0;
    let subtopicId = 0;

    getStore("session_practice").then(value => {
      console.log(value);
      value = JSON.parse(value);
      sessionId = value.sess_id;
      topicId = value.topic_id;
      subtopicId = value.subtopic_id;
      console.log(sessionId+"-"+topicId+"-"+subtopicId);
    });

    getStore("profile_user_id").then(value => {
      userId = value;
    });


    getStore("question_session").then(value => {
      let dataQuestion = JSON.parse(value);
      console.log(dataQuestion);
      let questionArr = [];

      let ansOrder = "";
        let ansChoosen = 0
        let quesAnswer = 0;

      for (var i = 0; i < dataQuestion.length; i++) {
        console.log(dataQuestion[i].key);
        ansOrder = "";
        ansChoosen = 0
        quesAnswer = 0;

        if (dataQuestion[i].questionData.answered_status == 0) {
          skipped++;
          quesAnswer = -1;
        } else {
          for (var j = 0; j < dataQuestion[i].questionData.answer.length; j++) {
            if (
              dataQuestion[i].questionData.answer[j].answered_status == 1 &&
              dataQuestion[i].questionData.answer[j].answer_status == true
            ) {
              ansChoosen = dataQuestion[i].questionData.answer[j].answer_id;
              correct++;
              quesAnswer = 1;
            } else if (
              dataQuestion[i].questionData.answer[j].answered_status == 1 &&
              dataQuestion[i].questionData.answer[j].answer_status == false
            ) {
              ansChoosen = dataQuestion[i].questionData.answer[j].answer_id;
              wrong++;
              quesAnswer = 0;
            }

            if(ansOrder == ""){
              ansOrder = dataQuestion[i].questionData.answer[j].answer_id;
            }else{
              ansOrder = ansOrder+","+dataQuestion[i].questionData.answer[j].answer_id;
            }
          }
        }
        console.log("resultquestionData" + ansOrder + "-" + ansChoosen + "-" + quesAnswer);
        questionArr.push({
          "answer_order":ansOrder,"answer_status":quesAnswer,"chosen_answer_id":ansChoosen,
          "parent_id":dataQuestion[i].questionData.question_id_parent,"question_id":dataQuestion[i].questionData.question_id,
          "time_taken":0,"topic_id":topicId,"subtopic_id":subtopicId})
      }
      let results = {"result":questionArr}
      // console.log(JSON.stringify(results));

      // console.log(JSON.stringify(results));
      console.log(`http://api.hometutor.com.my/mobile_api/index.php?process=practice_end_session&user_id=${userId}&session_id=${sessionId}&score=${correct*20}&question=${JSON.stringify(results)}`);

      fetch(`http://api.hometutor.com.my/mobile_api/index.php?process=practice_end_session&user_id=${userId}&session_id=${sessionId}&score=${correct*20}&question=${JSON.stringify(results)}`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({})
      })
        .then(res => res.json())
        .then(responseJson => {
          let statusUpdate = responseJson.items.status;
          console.log(responseJson);
          if(statusUpdate == 1){
            console.log('success');
          }
        })
        .catch(error => console.log(error));

      console.log("resultquestion" + skipped + "-" + correct + "-" + wrong);
      // return({item :{skipped: skipped,correct:correct,wrong:wrong}});
      let dataTemp = {
        item: { skipped: skipped, correct: correct, wrong: wrong }
      };
      console.log(dataTemp);
      setStore("summary_data", dataTemp);
      console.log("still in qp");
      Actions.summary();
    });

    // .then(dataQuestion => {
    //   console.log(dataQuestion);
    //   for(var i=0;i<dataQuestion.length;i++){
    //     console.log(dataQuestion[i].key);
    //     if(dataQuestion[i].questionData.answered_status == 0){
    //       skipped++;
    //     }else{

    //       for(var j=0;j<dataQuestion[i].questionData.answer.length;j++){
    //         if(dataQuestion[i].questionData.answer[j].answered_status == 1 && dataQuestion[i].questionData.answer[j].answer_status == true){
    //           correct++;
    //         }else if(dataQuestion[i].questionData.answer[j].answered_status == 1 && dataQuestion[i].questionData.answer[j].answer_status == false){
    //           wrong++;
    //         }
    //       }
    //     }
    //   }

    //   console.log("resultquestion"+skipped+"-"+correct+"-"+wrong);
    //   // return({item :{skipped: skipped,correct:correct,wrong:wrong}});
    //   let dataTemp = {item :{skipped: skipped,correct:correct,wrong:wrong}};
    //   console.log('datatemp'+dataTemp);
    //   setStore('summary_data',dataTemp);
    // });

    // return({item : {skipCount: getStore.skipped,correctCount:getStore.correct,wrongCount:getStore.wrong}});
    // return({item : getStore("question_session").then(value => {
    // console.log(value);
    // let dataQuestion = JSON.parse(value);

    //   for(var i=0;i<dataQuestion.length;i++){
    //     // console.log(dataQuestion[i].key);
    //     if(dataQuestion[i].questionData.answered_status == 0){
    //       skipped++;
    //     }else{

    //       for(var j=0;j<dataQuestion[i].questionData.answer.length;j++){
    //         if(dataQuestion[i].questionData.answer[j].answered_status == 1 && dataQuestion[i].questionData.answer[j].answer_status == true){
    //           correct++;
    //         }else if(dataQuestion[i].questionData.answer[j].answered_status == 1 && dataQuestion[i].questionData.answer[j].answer_status == false){
    //           wrong++;
    //         }
    //       }
    //     }
    //   }
    //   console.log(skipped+"-"+correct+"-"+wrong);
    // }).catch(error => {
    //   console.error(error);
    // })});
  };

  static title = "Scrollable top bar";
  static backgroundColor = "#3f51b5";
  static appbarElevation = 0;

  RoutePage(key, obj) {
    if (obj == undefined) {
      obj = {};
      obj.name = "Fakhri";
    }
    console.log("Nak tahu obj");
    console.log(key);
    console.log(obj);
    console.log(this.state);

    const questionText = obj.question_text;
    // const questionTextParent = obj.question_text_parent;
    const questionTextParent = obj.question_text_parent;
    return (
      <View style={{ width: "100%", alignItems: "center", height: "97%" }}>
        <View style={styles.cardContainer}>
          <View style={{ flexDirection: "row", width: "100%", padding: 10 }}>
            <View style={{ width: "80%" }}>
              <Text>
                {obj.question_code_parent == null ||
                obj.question_code_parent == ""
                  ? obj.question_code
                  : obj.question_code_parent + " - " + obj.question_code}
              </Text>
            </View>
            <View style={{ width: "10%", paddingLeft: 15 }}>
              <MaterialCommunityIcons name="information-outline" size={20} />
            </View>
            <View style={{ width: "10%", paddingLeft: 10 }}>
              <MaterialCommunityIcons name="flag" size={20} />
            </View>
          </View>
          <ScrollView>
            <View style={{ paddingLeft: 10, paddingRight: 10 }}>
              <View style={{ paddingBottom: 10 }}>
                <Text>Pilih jawapan yang paling tepat</Text>
              </View>
              <View style={{}}>
                <View
                  style={{
                    alignItems: "center",
                    marginLeft: 10,
                    marginRight: 10
                  }}
                ><ScrollView horizontal={true}> 
                {console.log(obj.question_image.path)}
                  {obj.question_image_parent.path != null && obj.question_image_parent.path.trim() != "" ? (
                    <Image
                      source={{ uri: obj.question_image_parent.path }}
                      width={obj.question_image_parent.width}
                      // style={{ maxWidth:'100%',width:obj.question_image_parent.width}}
                    />
                  
                  ) : null}
                  </ScrollView>
                </View>
                {/* <Text style={{}}>{obj.question_text_parent}</Text> */}
                <HTML html={questionTextParent} />
                {/* <WebView
                  javaScriptEnabled={true}
                  domStorageEnabled={true}
                  source={{ html: questionTextParent }}
                /> */}
                <View style={{ paddingTop: 15 }}>
                  <Text>Choose your answer for the question</Text>
                </View>
                <View>
                <ScrollView horizontal={true}> 
                  {obj.question_image.path != null && obj.question_image.path != "" ? (
                    <Image
                      source={{ uri: obj.question_image.path }}
                      // width={Dimensions.get("window").width}
                      // style={{ maxWidth:'100%',width:obj.question_image.width}}
                      width={obj.question_image_parent.width}
                    />
                  ) : null}
                  </ScrollView>
                </View>
                <View style={{ paddingLeft: 10, paddingRight: 10 }}>
                 {obj.question_text != "*" && obj.question_text != null && obj.question_text != "" ? <HTML html={questionText} /> : null }
                
                </View>
              </View>
            </View>
            <View style={{ padding: 10 }}>
              <View style={{ paddingTop: 20 }}>
                <Text>Options</Text>
              </View>
              <FlatList
                data={obj.answer}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => (
                  <View>
                    <TouchableOpacity
                      onPress={() =>
                        this.choosenAnswer(key, obj, item.answer_id)
                      }
                      activeOpacity={100}
                    >
                      {item.answered_status == 0 ? (
                        <View
                          style={{
                            flexDirection: "row",
                            width: "100%",
                            padding: 10,
                            borderWidth: 1,
                            borderRadius: 4,
                            borderColor: "silver",
                            marginTop: 10
                          }}
                        >
                          <View
                            style={{
                              width: "5%",
                              alignItems: "center",
                              justifyContent: "center"
                            }}
                          >
                            {item.answer_no == 1 ? (
                              <Text>A</Text>
                            ) : item.answer_no == 2 ? (
                              <Text>B</Text>
                            ) : item.answer_no == 3 ? (
                              <Text>C</Text>
                            ) : (
                              <Text>D</Text>
                            )}
                          </View>
                          <View style={{ width: "95%", paddingLeft: 10 }}>
                          {item.answer_file.path != null && item.answer_file.path != "" ? 
                          <Image
                             source={{ uri: item.answer_file.path }}
                             width={item.answer_file.path.width}
                           /> :<HTML html={item.answer_text} />
                          }
                          </View>
                        </View>
                      ) : (
                        <View
                          style={{
                            flexDirection: "row",
                            width: "100%",
                            padding: 10,
                            borderWidth: 2,
                            borderRadius: 4,
                            borderColor: "#6047ab",
                            marginTop: 10
                          }}
                        >
                          <View
                            style={{
                              width: "5%",
                              alignItems: "center",
                              justifyContent: "center"
                            }}
                          >
                            {item.answer_no == 1 ? (
                              <Text>A</Text>
                            ) : item.answer_no == 2 ? (
                              <Text>B</Text>
                            ) : item.answer_no == 3 ? (
                              <Text>C</Text>
                            ) : (
                              <Text>D</Text>
                            )}
                          </View>
                          <View style={{ width: "95%", paddingLeft: 10 }}>
                          {console.log("nak tau answer path")}
                          {console.log(item.answer_file.path)}
                          {item.answer_file.path != null && item.answer_file.path != "" ? 
                          <Image
                             source={{ uri: item.answer_file.path }}
                             width={item.answer_file.path.width}
                           /> :<HTML html={item.answer_text} />
                          }
                          </View>
                        </View>
                      )}
                    </TouchableOpacity>
                  </View>
                )}
              />
              <View />
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }

  componentDidMount() {
    // console.log("nak tau props item")
    // console.log(this.props.subject_id+"-"+this.props.level_id+"-"+this.props.topic_id)
    fetch(
      `http://api.hometutor.com.my/mobile_api/index.php?process=exercise&level_id=${
        this.props.level_id
      }&subject_id=${this.props.subject_id}&topic_id=${
        this.props.subtopic_id
      }&is_active=1`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({})
      }
    )
      .then(response => response.json())
      .then(responseJson => {
        var itemData = responseJson.items;
        this.state.routes.map(item => {
          let obj = {};
          obj.name = item.title;
          const pages = () => this.RoutePage(item.key, obj);
          item.page = pages;
          item.questionData = itemData.exercise[obj.name - 1];
          obj = item.questionData;
        });
        this.setState({
          isLoading: false,
          ...this.state
        });
      })
      .then(data => {
        // console.log("routessss"+this.state.routes);
        setStore("question_session", this.state.routes);
      })
      .catch(error => {
        console.error(error);
      });
  }

  choosenAnswer(key, data, selectedID) {
    data.answered_status = 1;
    data.answer.map(item => {
      item.answered_status = 0;
      if (item.answer_id == selectedID) {
        item.answered_status = 1;
      }
    });
    this.state.routes.map(item => {
      if (item.key == key) {
        const pages = () => this.RoutePage(item.key, data);
        item.page = pages;
        item.questionData.answer = data.answer;
      }
    });
    this.setState({ ...this.state });
    setStore("question_session", this.state.routes);
  }

  _handleIndexChange = index =>
    this.setState({
      index
    });

  _renderTabBar = props => (
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={styles.indicator}
      style={styles.tabbar}
      tabStyle={styles.tab}
      labelStyle={styles.label}
    />
  );

  renderTabTab = () => {
    const obj = Object.assign(
      {},
      ...this.state.routes.map(item => {
        return { [item.key]: item.page };
      })
    );

    return (_renderScene = SceneMap(obj));
  };

  render() {
    if (this.state.routes[0].page == null) {
      return (
        <View style={{ flex: 1, paddingTop: 20 }}>
          <ActivityIndicator />
        </View>
      );
    } else {
      return (
        <View style={styles.container2}>
          <View style={{}}>
            <TabView
              style={[styles.container, this.props.style]}
              navigationState={this.state}
              renderScene={this.renderTabTab()}
              renderTabBar={this._renderTabBar}
              onIndexChange={this._handleIndexChange}
              initialLayout={initialLayout}
            />
          </View>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container2: {
    width: "100%",
    alignItems: "center",
    height: "100%"
  },
  cardContainer: {
    width: "95%",
    height: "99%",
    backgroundColor: "white",
    borderRadius: 2,
    elevation: 5,
    marginTop: 10,
    marginBottom: 10
  },
  scene: {
    flex: 1
  },
  container: {
    flex: 1
  },
  tabbar: {
    backgroundColor: "#fff"
  },
  tab: {
    width: 50
  },
  indicator: {
    backgroundColor: "#6047ab"
  },
  label: {
    color: "black",
    fontWeight: "bold"
  }
});

export default QuestionPage;

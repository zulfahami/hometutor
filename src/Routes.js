import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, AlertIOS } from "react-native";
import { Router, Stack, Scene, ActionConst, Actions } from "react-native-router-flux";
import Modal from "react-native-modal";

import Ionicons from "react-native-vector-icons/Ionicons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { setStore,getStore } from "./live_api/Utilities";
import { Stopwatch, Timer } from 'react-native-stopwatch-timer'

import Login from "./pages/Login";
import Home from "./pages/Home";
import Practice from "./pages/Practice";
import Subjective from "./pages/Subjective";
import MenuDrawer from "./components/MenuDrawer";
import Report from "./pages/Report";
import Dashboard from "./pages/Dashboard";
import StudentProfile from "./pages/StudentProfile";
import ParentProfile from "./pages/ParentPofile";
import Subscription from "./pages/Subscription";
import Setting from "./pages/Setting";
import Download from "./pages/Download";
import QuestionInstruction from "./pages/QuestionInstruction";
import QuestionPage from "./pages/QuestionPage";
import Summary from "./pages/Summary";
import ScorePractice from "./pages/ScorePractice";
import ScoreSubjective from "./pages/ScoreSubjective";
import QuestionInstructionSubjective from "./pages/QuestionInstructionSubjective"
import QuestionPageSubjective from "./pages/QuestionPageSubjective";
import SummarySubjective from "./pages/SummarySubjective";
import SubjectiveReview from "./pages/SubjectiveReview";
import PractiveReview from "./pages/PracticeReview";
import CountUpTimer from "./components/CountUpTimer";
import ReportDetails from "./pages/ReportDetails";
import ReportDetailsSubjective from "./pages/ReportDetailsSubjective";
import SubtopicPractice from "./pages/SubtopicPractice";
import SubtopicSubjective from "./pages/SubtopicSubjective";



import Dialog, { SlideAnimation,DialogContent,DialogTitle } from 'react-native-popup-dialog';

const options = {
  container: {
    padding: 5,
    borderRadius: 5
  }
}




const summary = () => {
  const result = new QuestionPage;
  result.calculateData();
  // result.calculateData().then(value => { Actions.summary(); });

  // console.log("data ni keluar x? : "+rsCalcdata);
  // Actions.summary(rsCalcdata);
}

const summarySubjective = () => {
  const resultSubj = new QuestionPageSubjective;
  resultSubj.calculateDataSubj();
  // Actions.summarySubjective();
}
const toggleAlert = () =>{
  AlertIOS.alert(
    'Submit',
    'Are you sure want to submit this test?',
    [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {
        text: 'Submit',
        onPress: summary,
      },
    ],
  );
}

const toggleAlert2 = () =>{
  AlertIOS.alert(
    'Submit',
    'Are you sure want to submit this test?',
    [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {
        text: 'Submit',
        onPress: summarySubjective,
      },
    ],
  );
}
const MenuIcon = () => {
  return <Ionicons name="ios-menu" size={30} color="white" />;
};

const RightMenuIcon = () => {
  return (
    <View style={{ flexDirection: "row" }}>
      <TouchableOpacity onPress={Actions.subscription}>
        <View style={{ paddingRight: 20 }}>
          <MaterialCommunityIcons name="cart" size={26} color="white" />
        </View>
      </TouchableOpacity>

      <TouchableOpacity onPress={Actions.studentProfile}>
        <View style={{ paddingRight: 15 }}>
          <MaterialCommunityIcons name="account" size={26} color="white" />
        </View>
      </TouchableOpacity>
     {/* <Modal isVisible={this.state.isModalVisible}>
          <View style={{ flex: 1 }}>
            <Text>Hello!</Text>
            <TouchableOpacity onPress={this._toggleModal}>
              <Text>Hide me!</Text>
            </TouchableOpacity>
          </View>
        </Modal>` */}

    </View>
  );
};


const practice = (levelId,levelName) => {
  setStore('chosen_level_id',levelId);
  setStore('chosen_level_name',levelName);
  console.log("kkkkkkkkkkkk"+levelId+"-"+levelName);
  Actions.practice();
}

const subjective = (levelId,levelName) => {
  setStore('chosen_level_id',levelId);
  setStore('chosen_level_name',levelName);
  console.log("kkkkkkkkkkkkssss"+levelId+"-"+levelName);
  Actions.subjective();
}


const changeLevel = (moduleTemp) => {
  console.log('im in'+moduleTemp);
  getStore("level_list")
      .then(value => {
        let dataUser = JSON.parse(value);
        console.log(dataUser);

        let levelArr = [];

        if(moduleTemp == 'practice'){
        for(let x in dataUser){
          const item = dataUser[x];
          levelArr.push(
            {
              text: item.name_en,
              onPress:() => practice(item.id,item.name_en),
            }
          );
    
        }
      }else if(moduleTemp == 'subjective'){
        for(let x in dataUser){
          const item = dataUser[x];
          levelArr.push(
            {
              text: item.name_en,
              onPress:() => subjective(item.id,item.name_en),
            }
          );
    
        }
      }

        AlertIOS.alert(
          'Level Selection',
          // <View>
            // <Text>{dataUser[0].name_en}</Text>
            // <Text>{dataUser[1].name_en}</Text>
            // <Text>{dataUser[2].name_en}</Text>
          // </View>
          
          'Please select your level'
          ,
          // [
          //   // {
          //   //   text: 'Cancel',
          //   //   onPress: () => console.log('Cancel Pressed'),
          //   //   style: 'cancel',
          //   // },
          //   // {
          //   //   text: 'Submit',
          //   //   onPress: summary,
          //   // },
          //   {
          //     text: dataUser[0].name_en,
          //     onPress: summary,
          //   },
          //   {
          //     text: dataUser[1].name_en,
          //     onPress: summary,
          //   },
          //   {
          //     text: dataUser[2].name_en,
          //     onPress: summary,
          //   },
          // ],
          levelArr,
        );

        // return(
        // // tempFunc() { 
        // <Dialog
        //         visible={true}
        //         dialogTitle={<DialogTitle title="Select Level" />}
        //         width={0.5}
        //         height={0.5}
        //         onTouchOutside={() => { false }}
        //       >
        //         <DialogContent>
        //           <Text>Hello World</Text>
        //           {/* {levelArr} */}
        //         </DialogContent>
        //       </Dialog>
        // // }
        // );        

        // let levelArr = [];

        // for(let x in dataUser){
        //   const item = dataUser[x];
        //   levelArr.push(
        //     <View key={x} style={{width:'50%',}}>
        //       <Text style={{paddingTop:10}}>
        //         {item.name}
        //       </Text>
        //       </View>
        //   );
    
        // }


        // return(
          // levelArr.push
          // <Dialog
          //       visible={true}
          //       dialogTitle={<DialogTitle title="Select Level" />}
          //       width={0.5}
          //       height={0.5}
          //       onTouchOutside={() => { false }}
          //     >
          //       <DialogContent>
          //         <Text>Hello World</Text>
          //         {/* {levelArr} */}
          //       </DialogContent>
          //     </Dialog>
        // );

      })
      .catch(error => {
        console.error(error);
      });

      
      return(
        // tempFunc() { 
        <Dialog
                visible={true}
                dialogTitle={<DialogTitle title="Select Level" />}
                width={0.5}
                height={0.5}
                onTouchOutside={() => { false }}
              >
                <DialogContent>
                  <Text>Hello World</Text>
                  {/* {levelArr} */}
                </DialogContent>
              </Dialog>
        // }
        );   

}

const SubjectiveIcon = (level) => {
  // let levelName = "";
  // getStore("all_login_data")
  //     .then(value => {
  //       let dataUser = JSON.parse(value);
  //       console.log(dataUser);

  //       levelName = dataUser.level.name;

        

  //     })
  //     .catch(error => {
  //       console.error(error);
  //     });
    // if(getStore('chosen_level_name')){
      // level = getStore('chosen_level_name');
    // }   

    // getStore("chosen_level_name").then(value => {
    //   level = value
    // });

      return (
        <TouchableOpacity onPress={() => changeLevel('subjective') }>
          <View style={{ flexDirection: "row", padding: 5 }}>
            <View style={{ paddingRight: 5 }}>
              <Text>Change Level</Text>
            </View>
            <View style={{ paddingRight: 5 }}>
              <MaterialCommunityIcons name="pencil" size={16} />
            </View>
          </View>
        </TouchableOpacity>
      );

};

const PracticeIcon = (level) => {
  // let levelName = "";
  // getStore("all_login_data")
  //     .then(value => {
  //       let dataUser = JSON.parse(value);
  //       console.log(dataUser);

  //       levelName = dataUser.level.name;

        

  //     })
  //     .catch(error => {
  //       console.error(error);
  //     });

  // if(getStore('chosen_level_name')){
    // level = getStore('chosen_level_name');
  // }

  // getStore("chosen_level_name").then(value => {
  //   // level = value;
  //   let levelTempname = JSON.parse(value);
  //   console.log('fffffff'+levelTempname);

  //   return (
  //     <TouchableOpacity onPress={ changeLevel }>
  //       <View style={{ flexDirection: "row", padding: 5 }}>
  //         <View style={{ paddingRight: 5 }}>
  //           <Text>{levelTempname}</Text>
  //         </View>
  //         {/* <View style={{ paddingRight: 5 }}>
  //           <MaterialCommunityIcons name="pencil" size={16} />
  //         </View> */}
  //       </View>
  //     </TouchableOpacity>
  //   );

  // });
//   console.log('fffffff'+level);
//       return (
//         <TouchableOpacity onPress={ changeLevel }>
//           <View style={{ flexDirection: "row", padding: 5 }}>
//             <View style={{ paddingRight: 5 }}>
//               <Text>{level}</Text>
//             </View>
//             {/* <View style={{ paddingRight: 5 }}>
//               <MaterialCommunityIcons name="pencil" size={16} />
//             </View> */}
//           </View>
//         </TouchableOpacity>
//       );
      return (
        <TouchableOpacity onPress={() => changeLevel('practice') }>
          <View style={{ flexDirection: "row", padding: 5 }}>
            <View style={{ paddingRight: 5 }}>
              <Text>Change Level</Text>
            </View>
            <View style={{ paddingRight: 5 }}>
              <MaterialCommunityIcons name="pencil" size={16} />
            </View>
          </View>
        </TouchableOpacity>
      );

};

const questionPageLeft = (level) => {
  return(
    <View style={{paddingLeft:10}}>
      <View style={{flexDirection:'row'}}>
        <View>
          <MaterialCommunityIcons name="alarm-check" color="white" size={26} />
        </View>
        <View style={{paddingLeft:10, justifyContent:'center'}}>
       <CountUpTimer />
        </View>
      </View>
      </View>
  )
}

const questionPageRight = () => {
  return(
    <View style={{paddingRight:10}}><Text style={{color:'white'}} onPress={toggleAlert}>END TEST</Text></View>
  )
}

const questionPageLeft2 = () => {
  return(
    <View style={{paddingLeft:10}}>
      <View style={{flexDirection:'row'}}>
        <View>
          <MaterialCommunityIcons name="alarm-check" color="white" size={26} />
        </View>
        <View style={{paddingLeft:10, justifyContent:'center'}}>
        <CountUpTimer />
        </View>
      </View>
      </View>
  )
}

const questionPageRight2 = () => {
  return(
    <View style={{paddingRight:10}}><Text style={{color:'white'}} onPress={toggleAlert2}>END TEST</Text></View>
  )
}

const renderBackButton = () => {
  return (
    <TouchableOpacity onPress={Actions.home}>
      <View style={{ flexDirection: "row", padding: 5 }}>
        <View style={{ paddingLeft: 5 }}>
          <MaterialCommunityIcons name="chevron-left" size={20} />
        </View>
        {/* <View style={{ paddingRight: 5 }}>
          <MaterialCommunityIcons name="pencil" size={16} />
        </View> */}
      </View>
    </TouchableOpacity>
  );

};

export default class Routes extends Component {
  
  state = {
    isModalVisible: false,
    level:""
  };


  

  componentDidMount() {
    getStore("all_login_data")
      .then(value => {
        let dataUser = JSON.parse(value);
        console.log(dataUser);

        // levelName = dataUser.level;
        this.setState({ level: dataUser.level.name });
      })
      .catch(error => {
        console.error(error);
      });
  }


  _toggleModal = () =>
    this.setState({ isModalVisible: !this.state.isModalVisible });
    
  
  render() {
    return (
      <Router>
        <Scene key="root" hideNavBar  panHandlers={null} >
          <Scene key="login" component={Login} title="Login" initial={true} />
          <Scene
            key="practice"
            component={Practice}
            hideNavBar={false}
            renderRightButton ={PracticeIcon(this.state.level)}
            renderLeftButton={() => renderBackButton()}
          />
          <Scene
            key="subjective"
            component={Subjective}
            hideNavBar={false}
            renderRightButton={SubjectiveIcon(this.state.level)}
            title=""
            renderLeftButton={() => renderBackButton()}
          />
          <Scene
            key="dashboard"
            component={Dashboard}
            hideNavBar={false}
            title=""
          />
          <Scene
            key="report"
            component={Report}
            hideNavBar={false}
            title="Report"
            titleStyle={{ color: "white" }}
            navigationBarStyle={{ backgroundColor: "#6047ab" ,borderBottomColor: 'transparent'}}
          >
          </Scene>
          <Scene
            key="studentProfile"
            component={StudentProfile}
            hideNavBar={false}
            title=""
          />
          <Scene
            key="parentProfile"
            component={ParentProfile}
            hideNavBar={false}
            title=""
          />
          <Scene
            key="subscription"
            component={Subscription}
            hideNavBar={false}
            title=""
          />
          <Scene
            key="setting"
            component={Setting}
            hideNavBar={false}
            title=""
          />
          <Scene
            key="download"
            component={Download}
            hideNavBar={false}
            title=""
          />
          <Scene
            key="questionInstruction"
            component={QuestionInstruction}
            hideNavBar={false}
            title=""
          />
          <Scene 
            key="questionInstructionSubjective"
            component={QuestionInstructionSubjective}
            hideNavBar={false}
            title=""
          />
          <Scene
            key="questionPage"
            component={QuestionPage}
            hideNavBar={false}
            title=""
            navigationBarStyle={{ backgroundColor: "#6047ab" ,}}
            renderLeftButton={questionPageLeft}
            renderRightButton={questionPageRight}
          />
          <Scene
            key="questionPageSubjective"
            component={QuestionPageSubjective}
            hideNavBar={false}
            title=""
            navigationBarStyle={{ backgroundColor: "#6047ab" ,}}
            renderLeftButton={questionPageLeft2}
            renderRightButton={questionPageRight2}
          />
          <Scene 
            key="summary"
            component={Summary}
            hideNavBar={false}
            title="Summary"
            renderLeftButton={() => renderBackButton()}
          />
          <Scene 
            key="summarySubjective"
            component={SummarySubjective}
            hideNavBar={false}
            title="Summary"
            renderLeftButton={() => renderBackButton()}
          />
          <Scene 
            key="scorePractice"
            component={ScorePractice}
            hideNavBar={false}
            title=""
          />
          <Scene 
            key="scoreSubjective"
            component={ScoreSubjective}
            hideNavBar={false}
            title=""
          />
          <Scene 
            key="practiceReview"
            component={PractiveReview}
            hideNavBar={false}
            title=""
          />
          <Scene 
            key="subjectiveReview"
            component={SubjectiveReview}
            hideNavBar={false}
          />
           <Scene 
            key="countUpTimer"
            component={CountUpTimer}
            hideNavBar={false}
          />
          <Scene 
            key="reportDetails"
            component={ReportDetails}
            hideNavBar={false}
          />
          <Scene 
            key="reportDetailsSubjective"
            component={ReportDetailsSubjective}
            hideNavBar={false}
          />
          <Scene 
            key="subtopicPractice"
            component={SubtopicPractice}
            hideNavBar={false}
          />
          <Scene 
            key="subtopicSubjective"
            component={SubtopicSubjective}
            hideNavBar={false}
          />
          <Scene
            key="drawer"
            drawer
            drawerWidth={300}
            hideNavBar
            contentComponent={MenuDrawer}
            drawerIcon={MenuIcon}
          >
            <Scene
              key="home"
              component={Home}
              title="Home Tutor"
              titleStyle={{ color: "white" }}
              renderRightButton={RightMenuIcon}
              hideNavBar={false}
              drawerLockMode="locked-closed"
              navigationBarStyle={{
                backgroundColor: "blue"
              }}
            />
          </Scene>
        </Scene>
      </Router>
    );
  }
}
